@extends('admin.layout.admin')

@section('title', 'Cập nhật từ điển')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cập nhật từ điển
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Từ điển</a></li>
            <li><a href="#">Cập nhật từ điển</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cập nhật từ điển</h3>
                    </div>
                    <div class="box-body">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" id="progressBar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                <span class="sr-only">Tiến độ lấy dữ liệu</span>
                            </div>
                        </div>
                        <form action="{{ route('get_tu_dien') }}" method="get" id="form_get_html">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail1">Bắt đầu</label>
                                        <input type="number" class="form-control" id="start" placeholder="Bắt đầu" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail1">Kết thúc</label>
                                        <input type="number" class="form-control" id="end" placeholder="Kết thúc" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="button" style="float: right" id="btnGetJob" class="btn btn-primary" onclick="return getJobPartTime(this);">Cập nhật từ điển</button>
                                <input type="text" value="1" id="start" style="display: none">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script>
        function getJobPartTime(e){
            $(e).prop('disabled', true);
            $(e).text('Đang lấy dữ liệu việc làm...');
            // lấy thông tin submit
            var start = $('#start').val();
            var end = $('#end').val();

            getJob (start, end)
        }

        function getJob (start, end) {
            $('#progressBar').attr('style', 'width: '+ (parseInt(start)/parseInt(end) * 100) +'%');
            $.ajax({
                url: '{{ route('get_tu_dien') }}',
                method: 'get',
                data: {
                    start: start,
                    end: end,
                },
                success: function (data) {
                    var obj = jQuery.parseJSON(data);

                    if (obj.status == 500) {
                        $('#btnGetJob').prop('disabled', false);
                        $('#btnGetJob').text('Lấy thêm công việc');
                        alert('thành công');

                        return;
                    }

                    if (obj.status == 200) {
                        start = parseInt(start) + 5;
                        console.log(start);
                        return getJob(start , end);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    </script>
@endpush
