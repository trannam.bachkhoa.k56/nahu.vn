@extends('admin.layout.admin')

@section('title', 'Danh sách bài viết')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Bài viết
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách bài viết</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{ route('posts.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="posts" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Tiêu đề</th>
                                <th>Đường dẫn</th>
                                <th>Danh mục</th>
                                <th>Hình ảnh</th>
								<th>Lượt xem</th>
								<th>Chia sẻ</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tfoot>
                                <th width="5%">ID</th>
                                <th>Tiêu đề</th>
                                <th>Đường dẫn</th>
                                <th>Danh mục</th>
                                <th>Hình ảnh</th>
								<th>Lượt xem</th>
								<th>Chia sẻ</th>
                                <th>Thao tác</th>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
     @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection

@push('scripts')
<script>
    $(function() {
        var table = $('#posts').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('datatable_post') !!}',
            columns: [
                { data: 'post_id', name: 'post_id' },
                { data: 'title', name: 'title' },
                { data: 'slug', name: 'slug' },
                { data: 'category_string', name: 'category_string' },
                { data: 'image', name: 'image', orderable: false,
                    render: function ( data, type, row, meta ) {
                        return '<img src="'+data+'" width="100" />';
                    },
                    searchable: false  },
				{ data: 'views', name: 'views' },
				{ data: 'slug', name: 'slug', orderable: false,
					render: function ( data, type, row, meta ) {
						html = '<p><a href="/tin-tuc/'+ data +'" target="_blank">Xem trang</a></p>';
						html += `<iframe src="https://www.facebook.com/plugins/share_button.php?href=https://nahu.vn/tin-tuc/${data}&layout=button_count&size=small&appId=1875296572729968&width=102&height=20" width="102" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>`;

						html += `<div class="zalo-share-button" data-href="https://nahu.vn/tin-tuc/${data}" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false></div>`;

						return html;
					},
					searchable: false  },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    });
</script>
@endpush

