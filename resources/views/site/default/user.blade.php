@extends('site.layout.site')

@section('title','Thông tin tài khoản')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
<section class="pdtop30 content" >
    <div class="row">
        <div class="col-lg-3">
            <ul class="list-group">
                <li class="list-group-item active">Tìm gia sư</li>

                <li class="list-group-item">Nhận làm gia sư</li>
                <li class="list-group-item">
                    <div class="dropdown">
                        <a class="dropdown-toggle" type="button" id="dropdownAds" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Các dịch vụ được tài trợ
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownAds">
                            <a class="dropdown-item" href="#">Quảng cáo banner</a>
                            <a class="dropdown-item" href="#">Tin đăng tài trợ</a>
                            <a class="dropdown-item" href="#">Quảng cáo view trên nahu</a>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">Phân tích</li>
                <li class="list-group-item">
                   <div class="dropdown">
                        <a class="dropdown-toggle" type="button" id="dropdownPayment" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Tài chính
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownPayment">
                            <a class="dropdown-item" href="#">Giao dịch</a>
                            <a class="dropdown-item" href="#">Thanh toán</a>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">Hỗ trợ </li>
                <li class="list-group-item">
                    <div class="dropdown">
                        <a class="dropdown-toggle" type="button" id="dropdownInformation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tài khoản 
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownInformation">
                            <a class="dropdown-item" href="#">Thông tin tài khoản</a>
                            <a class="dropdown-item" href="#">Đổi mật khẩu</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="News bgrWhite pd15">
                <p>214124124</p>
            </div>
        </div>
    </div>
</section>
@endsection
