@extends('site.layout.site')

@section('type_meta', 'website')
@section('title', isset($category->title) && !empty($category->title) ? $category->title : 'Tin tức')
@section('meta_description',  isset($category->description) && !empty($category->description) ? $category->description : '')
@section('keywords', isset($category['meta_keyword']) && !empty($category['meta_keyword']) ? $category['meta_keyword'] : '')
@section('meta_image', !empty($category->image) ?  asset($category->image) : '' )
@section('meta_url', isset($category->slug) && !empty($category->slug) ?  '/danh-muc/'.$category->slug : '/danh-muc/tin-tuc')

@section('content')
    <section class="moreProduct listNews">
        <div class="container">
            <div class="link">
                <ol class="breadcrumb bgColor">
                    <li class="breadcrumb-item"><a href="/"><i class="fa fa-home" aria-hidden="true"></i> Trang chủ</a></li>
                    <li class="breadcrumb-item"><a href="#">{{ isset($category->title) ?$category->title : 'Tin tức' }}</a></li>
                </ol>
            </div>
            <div class="title">
                <h2 class="gray titl">{{ isset($category->title) ?$category->title : 'Tin tức' }}</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-3 col-sm-6 col-xs-12 mgb20">
                        <div class="listcate item">

                            <div class="img">
                                <div class="CropImg CropImgFull">
                                    <a href="/{{ isset($category->slug) ?$category->slug : 'tin-tuc' }}/{{ $post->slug }}" class="noDecoration thumbs">
                                        <img src="{{ isset($post['image']) ?  $post['image'] : '' }}"
                                             alt="{{ isset($post['title']) ?  $post['title'] : '' }}">
                                    </a>
                                </div>
                            </div>
                            <div class="newContent">
                                <a class="black" href="/{{ isset($category->slug) ?$category->slug : 'tin-tuc' }}/{{ $post->slug }}" >
                                    <p class="CutText2 mg0">{{ $post['title'] }}</p>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $posts->links() }}
        </div>
    </section>
@endsection

