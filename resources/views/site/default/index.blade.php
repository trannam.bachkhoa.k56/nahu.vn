@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
<div class="container">

    <section class="content">

        <div class="row">
            <div class="col-12 col-md-12">
                <p class="bgWhite help shadow"><i class="fa fa-ambulance colorGreen" aria-hidden="true"></i><i> Nhấn vào mục bên dưới để xem <a href="#">hướng dẫn</a> bạn nhé!</i></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12">
                <div class="tableLearn bgWhite shadow">
                    <form action="{!! route('learn_english') !!}" method="post" onSubmit="return learnEnglish(this);">
                        {!! csrf_field() !!}
                        <table class="table tableContent table-hover table-responsive" align="center" id="tableLearnWord">
                            <thead>
                            <tr>
                                <th scope="col" width="15%">
                                    Từ mới
                                </th>
                                <th scope="col" width="10%">Cách đọc</th>
                                <th scope="col" width="25%">Nghĩa của từ</th>
                                <th scope="col" width="30%">Ví dụ</th>
                                <th scope="col" width="10%" align="center">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!empty($dictionaries))
                                @foreach ($dictionaries as $dictionary)
                                    <tr>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input"  name="dictionary_id[]" value="{!! $dictionary->dictionary_id !!}" id="customCheck{!! $dictionary->dictionary_id !!}" checked>
                                                <label class="custom-control-label" for="customCheck{!! $dictionary->dictionary_id !!}">{!! $dictionary->word !!}</label>
                                            </div>
                                        </td>
                                        <td>
                                            <p>uk: <audio controls>
                                                    <source src="{!! $dictionary->uk_audio !!}" type="audio/mpeg">
                                                </audio> {!! $dictionary->uk_pron !!} </p>
                                            <p>us: <audio controls>
                                                    <source src="{!! $dictionary->us_audio !!}" type="audio/mpeg">
                                                </audio> {!! $dictionary->us_pron !!} </p>
                                        </td>
                                        <td>
                                            <div class="wordMean">
                                                {!! $dictionary->mean !!}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="wordMean">
                                                {!! $dictionary->example !!}
                                            </div>
                                        </td>
                                        <td  align="center">
                                            <span class="point" onClick="return removeWord(this);" dictionary_id="{!! $dictionary->dictionary_id !!}"> Xóa</span>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <td colspan="6">
                                <button class="btn bg btn-success btnSubmit">HỌC TỪ MỚI</button>
                            </td>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </section>

</div>
<script>
    function learnEnglish(e) {
        var numberChecked =  $('input:checkbox:checked').length;
        if (numberChecked < 5) {
            alert('Vui lòng học tối thiểu 5 từ');

            return false;
        }

        return true;
    }
</script>
@endsection

