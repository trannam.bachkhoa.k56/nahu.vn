@extends('site.layout.site')

@section('type_meta', 'website')
@section('title', 'Câu hỏi')
@section('meta_description', 'Câu trả lời')
@section('keywords', 'Câu hỏi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="question bgWhite">
                    <form action="/ket-qua" method="post" >
                        {!! csrf_field() !!}
                        <h1 class="question-title colorGreen txtCenter">ÔN LUYỆN TỪ MỚI NHÉ!</h1>
                        <p class="colorGreen txtCenter"><i>Con đường thành công không có dấu chân của kẻ lười biếng!</i></p>
                        <p class="colorGreen txtCenter colorRed"> 15:40 phút</p>
                        @foreach ($questions as $id => $question)
                            <div class="questionDetail {!! $id == 0  ? 'active' : '' !!}">
                                <p class="colorGreen txtCenter colorRed">{!! ($id+1) !!}/{!! count($questions) !!} câu</p>
                                <div class="body-question shadow">
                                    <div class="wordMean">
                                        <p >Câu {!! ($id+1) !!}: Chọn đáp án tương ứng với: {!! $question['question'] !!}</p>
                                    </div>
                                </div>
                                @foreach ($question['answer'] as $idAnswer => $answer)
                                    <div class="body-question shadow">
                                        <div class="custom-control custom-checkbox wordMean">
                                            <input type="checkbox" class="custom-control-input" name="answer[]" value='{!! $idAnswer !!}' id="{!! ($id+1) !!}{!! ($idAnswer+1) !!}">
                                            <label class="custom-control-label" for="{!! ($id+1) !!}{!! ($idAnswer+1) !!}">{!! ($idAnswer+1) !!}. {!! $answer !!}</label>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="footer-question">
                                    @if ( (count($questions) -1) > $id )
                                    <span class="btn bg btn-success" onClick="return prev(this);">Quay lại</span>
                                    <span class="btn bg btn-success" onClick="return next(this);">Tiếp tục</span>
                                        @else
                                        <button type="submit" class="btn bg btn-success">Xem kết quả</button>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function next(e) {
            $(e).parent().parent().hide();
            $(e).parent().parent().next().show();
        }
        function prev(e) {
            $(e).parent().parent().hide();
            $(e).parent().parent().prev().show();
        }
    </script>
    <style>
        .questionDetail {
            display: none;
        }
        .questionDetail.active {
            display: block;
        }
    </style>
@endsection