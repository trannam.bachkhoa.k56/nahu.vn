@extends('site.layout.site')

@section('type_meta', 'website')
@section('title', 'Câu hỏi')
@section('meta_description', 'Câu trả lời')
@section('keywords', 'Câu hỏi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="question bgWhite">
                    <h1 class="question-title colorGreen txtCenter">KẾT QUẢ ÔN LUYỆN CỦA BẠN</h1>
                    <div class="result-question shadow">
                        <p>Số câu trả lời đúng: {!! $point !!} / {!! $numberQuestions !!} câu.</p>
                        <p>Thời gian làm đề: 15:00 phút.</p>
                        <p>Đánh giá: rất tốt</p>
                    </div>
                    <div class="footer-question">
                        <a href="/" class="btn bg btn-success">Làm lại</a>
                        <a href="{!! route('clean_word') !!}" class="btn bg btn-success">Học từ mới</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection