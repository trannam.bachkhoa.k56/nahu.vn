<footer class="bgWhite">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="box-title">
                    <h3>TÌM KIẾM CHÚNG TÔI TRÊN FACEBOOK</h3>
                </div>
                <div class="box-content">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/pg/nahuvietnam/&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1875296572729968" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box-title">
                    <h3>CHĂM SÓC KHÁCH HÀNG</h3>
                </div>
                <div class="box-content">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Hướng dẫn sử dụng</li>
                        <li class="list-group-item">Thanh toán</li>
                        <li class="list-group-item">Chính sách hoàn tiền</li>
                        <li class="list-group-item">Chăm sóc khách hàng</li>
                        <li class="list-group-item">Hướng dẫn mua hàng</li>
                        <li class="list-group-item">Bảng giá</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="box-title">
                    <h3>TIN TỨC NỔI BẬT</h3>
                </div>
                <div class="box-content">
                    <ul class="list-group list-group-flush">
                        @foreach (\App\Entity\Post::newPost(5) as $post)
                        <li class="list-group-item">
                            <div class="media mt-3">
                                <a class="mr-3" href="/tin-tuc/{!! $post->slug !!}">
                                    <img src="{!! !empty($post->image) ? asset($post->image) : $information['logo'] !!}" class="mr-3" alt="{!! $post->title !!}" width="100">
                                </a>
                                <div class="media-body">
                                    <p class="mt-0"><a href="/tin-tuc/{!! $post->slug !!}" >{!! $post->title !!}</a></p>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<p class="coppyRight">
    Bản quyền thuộc về nahu.vn
</p>