<header class="bgColor">
    <section class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-light bgColor">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="/">Trang chủ</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="/danh-muc/tin-tuc">Tin tức</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="/trang/thanh-toan">Thanh toán</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Hỏi đáp</a>
                            </li>
                        </ul>
                        <div class="form-inline my-2 my-lg-0">
                            <ul class="navbar-nav mr-auto">
                                @if (\Illuminate\Support\Facades\Auth::check())
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">Hi, {!! \Illuminate\Support\Facades\Auth::user()->name !!}</a>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link" href="{{ route('logoutHome') }}">Đăng xuất</a>
                                    </li>
                                    @else
                                <li class="nav-item active">
                                    <a class="nav-link" href="/trang/dang-nhap">Đăng nhập</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="/trang/dang-ky">Đăng ký</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </section>
</header>

<section class="belowHeader">
    <section class="container">
        <div class="row">
            <div class="col-2 col-md-2">
                <a href="/"><img src="{!! isset($information['logo']) ? $information['logo'] : '' !!}" alt="nahu.vn" title="nahu.vn" width="100%"/></a>
            </div>
            <div class="col-10 col-md-10">
                <div class="form-inline formSearchWord justify-content-end">
                    <input class="form-control "  id="anythingSearch" type="search" placeholder="Tìm kiếm tiếng anh" aria-label="Search">
                    <button class="btn btnSearch" type="submit" onClick="return search(this);"><i class="fa fa-search" aria-hidden="true" ></i></button>
                </div>
                <div id="searchShow">

                </div>
            </div>
        </div>
    </section>
</section>
<script>
    $(document).ready(function () {
        $("#anythingSearch").on("keyup", function () {
            var value = $(this).val().toLowerCase();

            /* ajax gọi từ mới ra */
            $("#searchShow").show();
            $.ajax({
                type: "get",
                url: '{!! route('show_word_search') !!}',
                data: {
                    word: value
                },
                success: function(data) {
                    $("#searchShow").empty();
                    var obj = jQuery.parseJSON(data);

                    $.each( obj.dictionaries, function( index, element) {
                        var htmlWord = `<p  class="point" dictionary_id="${element.dictionary_id}"onClick="return selectWord(this);">${element.word}</p>`;

                        $("#searchShow").append(htmlWord);
                    });
                }
            });
            return false;

            if (value == '') {
                $("#searchShow").hide();
            }
        });
    });

    function search (e) {
        var value = $(e).prev().val().toLowerCase();
        $("#searchShow").hide();
        /* ajax gọi từ mới ra */
        $.ajax({
            type: "get",
            url: '{!! route('show_word_search') !!}',
            data: {
                word: value,
                search: 1
            },
            success: function(data) {
                $("#searchShow").empty();
                var obj = jQuery.parseJSON(data);

                if (obj.status == 500) {
                    alert('Không tìm thấy từ mới!');

                    return 0;
                }

                if (obj.status == 300) {
                    alert('Từ mới đã được thêm');

                    return 0;
                }
                var html = `<tr>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="dictionary_id[]" value="${obj.dictionary.dictionary_id}" id="worDictionarie${obj.dictionary.dictionary_id}" checked>
                                        <label class="custom-control-label" for="worDictionarie${obj.dictionary.dictionary_id}">${obj.dictionary.word}</label>
                                    </div>
                                </td>
                                <td>
                                    <p>uk: <audio controls>
                                          <source src="${obj.dictionary.uk_audio}" type="audio/mpeg">
                                        </audio> ${obj.dictionary.uk_pron} </p>
                                    <p>us: <audio controls>
                                          <source src="${obj.dictionary.us_audio}" type="audio/mpeg">
                                        </audio> ${obj.dictionary.us_pron} </p>
                                </td>
                                <td>
                                    <div class="wordMean">
                                        ${obj.dictionary.mean}
                                    </div>
                                </td>
                                <td>
                                    <div class="wordMean">
                                        ${obj.dictionary.example}
                                    </div>
                                </td>
                                <td  align="center">
                                    <span class="point" onClick="return removeWord(this);" dictionary_id="${obj.dictionary.dictionary_id}">Xóa</span>
                                </td>
                            </tr>`;

                $('#tableLearnWord tbody').prepend(html);
            }
        });
        return false;

        if (value == '') {
            $("#searchShow").hide();
        }
    }
    function selectWord(e) {
        $("#searchShow").hide();
        $("#anythingSearch").val("");

        $.ajax({
            type: "get",
            url: '{!! route('get_detail_word') !!}',
            data: {
                dictionary_id: $(e).attr('dictionary_id')
            },
            success: function(data) {
                $("#searchShow").empty();
                var obj = jQuery.parseJSON(data);

                if (obj.status == 500) {
                    return ;
                }

                var html = `<tr>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="dictionary_id[]" value="${obj.dictionary.dictionary_id}" id="worDictionarie${obj.dictionary.dictionary_id}" checked>
                                        <label class="custom-control-label" for="worDictionarie${obj.dictionary.dictionary_id}">${obj.dictionary.word}</label>
                                    </div>
                                </td>
                                <td>
                                    <p>uk: <audio controls>
                                          <source src="${obj.dictionary.uk_audio}" type="audio/mpeg">
                                        </audio> ${obj.dictionary.uk_pron} </p>
                                    <p>us: <audio controls>
                                          <source src="${obj.dictionary.us_audio}" type="audio/mpeg">
                                        </audio> ${obj.dictionary.us_pron} </p>
                                </td>
                                <td>
                                    <div class="wordMean">
                                        ${obj.dictionary.mean}
                                    </div>
                                </td>
                                <td>
                                    <div class="wordMean">
                                        ${obj.dictionary.example}
                                    </div>
                                </td>
                                <td  align="center">
                                    <span class="point" onClick="return removeWord(this);" dictionary_id="${obj.dictionary.dictionary_id}">Xóa</span>
                                </td>
                            </tr>`;

                $('#tableLearnWord tbody').prepend(html);
            }
        });
    }

    function removeWord(e) {
        $(e).parent().parent().remove();
        $.ajax({
            type: "get",
            url: '{!! route('delete_word') !!}',
            data: {
                dictionary_id: $(e).attr('dictionary_id')
            },
            success: function(data) {
                $(e).parent().parent().remove();
            }
        });
    }
</script>