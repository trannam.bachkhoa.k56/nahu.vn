@extends('site.layout.site')

@section('title', 'Khôi phục mật khẩu')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')


@section('content')
    <div class="container">

        <section class="content">

            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="box bgWhite shadow">
                        <h1 class="box-title" >Đăng nhập</h1>
                        <div class="col-12 offset-0  offset-md-2 col-md-8 col-lg-6 offset-lg-3">
                            <div class="box-content">
                                <p class="">Bạn vui lòng check lại email để khôi phục mật khẩu mới!</p>
                            </div>
                        </div><!--end: .contact-info-->
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection

