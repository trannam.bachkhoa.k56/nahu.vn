@extends('site.layout.site')

@section('title', 'Đăng nhập')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')


@section('content')
    <div class="container">

        <section class="content">

            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="box bgWhite shadow">
                        <h1 class="box-title" >Đăng nhập</h1>
                        <div class="col-12 offset-0  offset-md-2 col-md-8 col-lg-6 offset-lg-3">
                            <div class="box-content">
                                <form  action="/dang-nhap" method="post" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Tài khoản</span><span class="clred pd-05">(*)</span></label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control f14" name="email" placeholder="Tài khoản đăng nhập" value="{{ old('name') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Mật khẩu</span><span class="clred pd-05">(*)</span></label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control f14" name="password" placeholder="Mật khẩu đăng nhập" value="" required>
                                        </div>

                                    </div>
                                    <div class="form-group">

                                        @if($errors->any())
                                            @foreach ($errors->all() as $error)
                                                <div class="alert alert-danger" role="alert">
                                                    <strong>{!! $error !!}</strong>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 pdtop30">
                                            <button type="submit" class="btn btn-primary">Đăng nhập</button>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-8 offset-4">
                                            <span class="left mgright20"><a href="/trang/quen-mat-khau" title="Quên mật khẩu">Quên mật khẩu ? </a></span>
                                            <span class="right"><a href="dang-ky" title="">Bạn chưa có tài khoản ? </a></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div><!--end: .contact-info-->
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection

