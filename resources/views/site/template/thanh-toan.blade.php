@extends('site.layout.site')

@section('title', 'Nâng cấp tài khoản')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')


@section('content')
    <div class="container">

        <section class="content">

            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="box bgWhite shadow">
                        <h1 class="box-title" >Nâng cấp tài khoản</h1>
                        <div class="col-12 col-md-12">
                            <div class="">
                                <div class="PayscienceBottom clblack text-lt mbpdleft0 mg-20 pd-20" >
                                    <div class="content text-ct  col-12 pd-20 ">
                                        <p class="f24 text-up">Bạn đã học quá số lần giới hạn của nahu.vn rồi phải không? </p>
                                        <p>Nâng cấp nahu.vn để hưởng nhiều lợi ích của nahu.vn nhé!</p>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <div class="itemsologan">
                                                <ul>
                                                    <li>
                                                        <i class="fa fa-star pdright5" aria-hidden="true"></i>
                                                        <span class="f16">Học không giới hạn từ mới.</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <div class="itemsologan">
                                                <ul>
                                                    <li>
                                                        <i class="fa fa-star pdright5" aria-hidden="true"></i>
                                                        <span class="f16">Giúp bạn xin việc dễ dàng hơn!</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <div class="itemsologan">
                                                <ul>
                                                    <li>
                                                        <i class="fa fa-star pdright5" aria-hidden="true"></i>
                                                        <span class="f16">Bạn có thể đi đến bất kỳ đâu trên thế giới!</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-ct">
                                        <p class="pdbottom20 f14"></p>
                                    </div>

                                    <div class="border"></div>
                                    <div class="content text-ct  col-12 pd-20">
                                        <h3 class="f24 text-b700 text-up pdtop15 ">Chọn gói nạp tiền phù hợp</h3>
                                    </div>

                                    <div class="row mgtop30 selectCombo">
                                        <div class="col-xl-2 col-lg-0 col-md-0 col-sm-12 itemps"></div>
                                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 itemps">
                                            <div class="itemcombo text-ct">
                                                <div class="top clwhite pd-20">
                                                    <div class="number">
                                                        <span class="f38 text-b700">60.000</span>
                                                        <span>VNĐ</span>
                                                    </div>
                                                    <p class="price">1 Tháng</p>
                                                </div>
                                                <div class="bottom">
                                                    <p class="sale text-b700">Tiết kiệm 0%</p>
                                                    <form method="get" action="/thanh-toan-qua-chuyen-khoan">
                                                        <input type="hidden" value="60000" class="pricePayment" name="payment" />
                                                        <input type="hidden" value="1 tháng nâng cấp tài khoản" class="messagePayment" name="message" />
                                                        <button type="submit" class="pd-4 w80 text-up clwhite mgbottom20 f16 buttoncombo ds-inline button_opative buyXu" >Chọn</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 itemps">
                                            <div class="maxbuy">
                                                <div class="postop bgxanhlacay text-ct">
                                                    <span class="clwhite f14 text-up ds-block pd-5">Mua nhiều nhất</span>
                                                </div>
                                            </div>
                                            <div class="itemcombo text-ct">
                                                <div class="top clwhite pd-20">
                                                    <div class="number">
                                                        <span class="f38 text-b700">300.000</span>
                                                        <span>VNĐ</span>
                                                    </div>
                                                    <p class="price">6 tháng</p>
                                                </div>
                                                <div class="bottom">
                                                    <p class="sale text-b700">Tiết kiệm 60.000 VNĐ</p>
                                                    <form method="get" action="/thanh-toan-qua-chuyen-khoan">
                                                        <input type="hidden" value="300000" class="pricePayment" name="payment" />
                                                        <input type="hidden" value="6 tháng nâng cấp tài khoản" class="messagePayment" name="message" />
                                                        <button type="submit" class="pd-4 w80 text-up clwhite mgbottom20 f16 buttoncombo ds-inline button_opative buyXu" >Chọn</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 itemps">
                                            <div class="itemcombo text-ct">
                                                <div class="top clwhite pd-20">
                                                    <div class="number">
                                                        <span class="f38 text-b700">500.000</span>
                                                        <span>VNĐ</span>
                                                    </div>
                                                    <p class="price">1 năm</p>
                                                </div>
                                                <div class="bottom">
                                                    <p class="sale text-b700">Tiết kiệm 220.000 VNĐ</p>
                                                    <form method="get" action="/thanh-toan-qua-chuyen-khoan">
                                                        <input type="hidden" value="500000" class="pricePayment" name="payment" />
                                                        <input type="hidden" value="1 năm nâng cấp tài khoản" class="messagePayment" name="message" />
                                                        <button type="submit" class="pd-4 w80 text-up clwhite mgbottom20 f16 buttoncombo ds-inline button_opative buyXu" >Chọn</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 f14 mg-15 text-ct">
                                        <p>
                                            <i class="fa fa-info-circle pdright5" aria-hidden="true">
                                            </i>Nếu bạn có bất cứ thắc mắc nào về thanh toán vui lòng liên hệ: 097.456.1735
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div><!--end: .contact-info-->
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection

