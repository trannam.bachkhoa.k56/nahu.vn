@extends('site.layout.site')

@section('title', 'Hoàn tất thanh toán')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')


@section('content')
    <div class="container">
        <section class="content">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="box bgWhite shadow">
                        <h1 class="box-title" >Hoàn tất thanh toán</h1>
                        <div class="col-12 offset-0 col-md-12">
                            <div class="box-content">
                                <p>Để đơn hàng được khởi tạo, quý khách vui lòng thực hiện việc chuyển khoản tổng tiền là <span style="color: orange; font-weight: bold;">{{ number_format($payment-1000) }} đ</span> vào một trong những tài khoản ngân hàng hiển thị ngay bên dưới.</p>
                                <p>Trong nội dung chuyển khoản quý khách vui lòng điền mã đơn hàng là <span style="color: orange; font-weight: bold;">nahunc{!! $orderId !!}</span></p>

                                <p>Nếu quý khách không thể ghi mã đơn hàng vào nội dung chuyển khoản, quy khách vui lòng chụp hình ủy nhiệm chi và gửi vào mail: vn3ctran@gmail.com với tựa đề: Ủy nhiệm đơn hàng <span style="color: orange; font-weight: bold;">nahunc{!! $orderId !!}</span></p>
                                <div class="col-xs-12 col-md-12">
                                    <table  class="table table-bordered table-striped">
                                        <tr>
                                            <td width="20%">
                                                <img src="https://vaytiendoctordong.com/wp-content/uploads/2019/01/ng%C3%A2n-h%C3%A0ng-VCB.jpg" title="BIDV" width="150"/>
                                            </td>
                                            <td>
                                                <h4>Ngân hàng Vietcombank - chi nhánh ba đình</h4>
                                                <p>Chủ tài khoản: Trần Hải Nam</p>
                                                <p>Số tài khoản: 0611001948477</p>
                                                <p>Nội dung chuyển khoản: <span style="color: orange; font-weight: bold;">nahunc{!! $orderId !!}</span></p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

