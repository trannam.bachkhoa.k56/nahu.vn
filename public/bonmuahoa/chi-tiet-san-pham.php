<!DOCTYPE html>
<html lang="vi">
   <head>
      <meta name="google-site-verification" content="JvFQBx287KYQq21gfAKlaivAhmjOTsevbO0PZGkDBgA" />
      <meta http-equiv="content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <title>
         NÉT DUYÊN NGẦM
         Bốn Mùa Hoa			
      </title>
      <!-- ================= Page description ================== -->
      <meta name="description" content="GIỎ HOA TƯƠI – N&#201;T DUY&#202;N NGẦM Với sự kết hợp đơn giản đầy tinh tế của Hồng trứng g&#224; v&#224; Hồng đỏ, Giỏ hoa tươi N&#201;T DUY&#202;N NGẦM gợi l&#234;n n&#233;t đẹp duy&#234;n d&#225;ng của người phụ nữ. Những nụ Hồng trứng tr&#242;n trịa xếp kh&#237;t nhau như sự kh&#233;p n&#233;p, e ấp đầy nữ t&#237;nh m&#224; tạo h&#243;a đ&#227; ban tặng ri&#234;ng cho ph&#225;i đẹp. T&#236;nh y&#234;u m&#227;nh liệt của hoa Hồn">
      <!-- ================= Meta ================== -->
      <meta name="keywords" content="NÉT DUYÊN NGẦM, Giỏ hoa chúc mừng, Giỏ hoa yêu thương, Giỏ hoa chúc mừng, Giỏ hoa tươi, Sản phẩm mới, Giỏ hoa tươi, Hoa chúc mừng, Hoa yêu thương, Bốn Mùa Hoa, bonmuahoa.com">
      <link rel="canonical" href="net-duyen-ngam">
      <meta name='revisit-after' content='1 days' />
      <meta name="robots" content="noodp,index,follow" />
      <!-- ================= Favicon ================== -->
      <link rel="icon" href="100/145/391/themes/656787/assets/favicon.png?1532588934329" type="image/x-icon" />
      <!-- ================= Google Fonts ================== -->
      <link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css' />
      <!-- Facebook Open Graph meta tags -->
      <meta property="og:type" content="product">
      <meta property="og:title" content="NÉT DUYÊN NGẦM">
      <meta property="og:image" content="http:/thumb/grande/100/145/391/products/bmh-028-jpeg.jpg?v=1529976505177">
      <meta property="og:image:secure_url" content="https:/thumb/grande/100/145/391/products/bmh-028-jpeg.jpg?v=1529976505177">
      <meta property="og:price:amount" content="370.000">
      <meta property="og:price:currency" content="VND">
      <meta property="og:description" content="GIỎ HOA TƯƠI – NÉT DUYÊN NGẦM Với sự kết hợp đơn giản đầy tinh tế của Hồng trứng gà và Hồng đỏ, Giỏ hoa tươi NÉT DUYÊN NGẦM gợi lên nét đẹp duyên dáng của người phụ nữ. Những nụ Hồng trứng tròn trịa xếp khít nhau như sự khép nép, e ấp đầy nữ tính mà tạo hóa đã ban tặng riêng cho phái đẹp. Tình yêu mãnh liệt của hoa Hồn">
      <meta property="og:url" content="net-duyen-ngam">
      <meta property="og:site_name" content="Bốn Mùa Hoa">
      <!-- Plugin CSS -->			
      <link rel="stylesheet" href="font-awesome/4.5.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="ionicons/2.0.1/css/ionicons.min.css" >
      <link rel="stylesheet" href="themify-icons/0.1.2/css/themify-icons.css" >
      <script>
         var template = 'product';
          
      </script>
      <!-- Build Main CSS -->	
      <link href='100/145/391/themes/656787/assets/plugin.scss.css?1532588934329' rel='stylesheet' type='text/css' />
      <link href='100/145/391/themes/656787/assets/base.scss.css?1532588934329' rel='stylesheet' type='text/css' />
      <link href='100/145/391/themes/656787/assets/style.scss.css?1532588934329' rel='stylesheet' type='text/css' />
      <link href='100/145/391/themes/656787/assets/module.scss.css?1532588934329' rel='stylesheet' type='text/css' />
      <link href='100/145/391/themes/656787/assets/responsive.scss.css?1532588934329' rel='stylesheet' type='text/css' />
      <!-- End-->
      <!-- Bizweb conter for header -->
      <script>
         var Bizweb = Bizweb || {};
         Bizweb.store = 'hoa-kieng-an-loc.bizwebvietnam.net';
         Bizweb.id='145391';
         Bizweb.theme = {"id":656787,"role":"main","name":"TL Cosmetics"};
         Bizweb.template = 'product';
      </script>
      <script>
         (function() {
         function asyncLoad() {
         var urls = ["https://memberdeals.bizwebapps.vn/scripts/appbulk_member_deals.min.js?store=hoa-kieng-an-loc.bizwebvietnam.net","//promotionpopup.bizwebapps.vn/genscript/script.js?store=hoa-kieng-an-loc.bizwebvietnam.net","https://bizwebform.bizwebapps.vn/genscript/script.js?store=hoa-kieng-an-loc.bizwebvietnam.net","//productreviews.bizwebapps.vn/assets/js/productreviews.min.js?store=hoa-kieng-an-loc.bizwebvietnam.net"];
         for (var i = 0; i < urls.length; i++) {
         var s = document.createElement('script');
         s.type = 'text/javascript';
         s.async = true;
         s.src = urls[i];
         s.src = urls[i];
         var x = document.getElementsByTagName('script')[0];
         x.parentNode.insertBefore(s, x);
         }
         }
         window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);
         })();
      </script>
      <script type='text/javascript'>
         (function() {
         var log = document.createElement('script'); log.type = 'text/javascript'; log.async = true;
         log.src = '//stats.bizweb.vn/delivery/145391.js?lang=vi';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(log, s);
         })();
      </script>
      <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         
         ga('create', 'UA-100770668-1', 'auto');
         ga('send', 'pageview');
         
      </script>
      <script>var ProductReviewsAppUtil=ProductReviewsAppUtil || {};</script>
   </head>
   <body>
      <div class="page-body">
         <div class="hidden-md hidden-lg opacity_menu"></div>
         <div class="opacity_filter"></div>
         <div class="body_opactiy"></div>
         <!-- Main content -->
         <div class="topbar">
            <div class="container">
               <div class="row">
                  <div class="col-lg-7 col-md-7 col-sm-9 hidden-xs">
                     <div class="conatct_top">
                        <div class="hot_news_side">
                           <span class="title_hot_news"><i class="fa fa-star"></i>Tin mới</span>
                           <div class="ticker">
                              <marquee behavior="scroll" direction="left" 
                                 onmouseover="this.stop();" 
                                 onmouseout="this.start();">
                                 <a class="title_ticker" title="2 cách cắm hoa Tết đẹp lung linh dành cho gia đình trẻ với chi phí chỉ khoảng 1 triệu đồng" href="2-cach-cam-hoa-tet-dep-lung-linh-danh-cho-gia-dinh-tre-voi-chi-phi-chi-khoang-1-trieu-dong">2 cách cắm hoa Tết đẹp lung linh dành cho gia đình trẻ với chi phí chỉ khoảng 1 triệu đồng</a>
                                 <a class="title_ticker" title="20/11 không cần ra tiệm vì đã có 5 cách gói hoa vừa đẹp vừa đơn giản này" href="20-11-khong-can-ra-tiem-vi-da-co-5-cach-goi-hoa-vua-dep-vua-don-gian-nay">20/11 không cần ra tiệm vì đã có 5 cách gói hoa vừa đẹp vừa đơn giản này</a>
                                 <a class="title_ticker" title="3 cách cắm hoa trang trí nhà đẹp từ những vật dụng thường ngày" href="3-cach-cam-hoa-trang-tri-nha-dep-tu-nhung-vat-dung-thuong-ngay">3 cách cắm hoa trang trí nhà đẹp từ những vật dụng thường ngày</a>
                              </marquee>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">
                     <div class="_login">
                        <i class="ion-android-person-add"></i>
                        <div class="use">
                           <a href="account/register" title="Đăng ký">Đăng ký</a>
                           <a href="account/login"  title="Đăng nhập">Đăng nhập</a>
                        </div>
                     </div>
                     <div class="top_link_right">
                        <a href="apps/kiem-tra-don-hang" title="Kiểm tra đơn hàng"><i class="ion-clipboard"></i> </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="wrap_header_top">
            <div class="header_top">
               <div class="topbar_wrap">
                  <div class="container">
                     <div class="row">
                        <div class="head_content col-lg-12 col-md-12 col-sm-12">
                           <div class="row">
                              <div class="menu-bar hidden-md hidden-lg">
                                 <a href="#nav-mobile">
                                 <i class="fa fa-align-justify"></i>
                                 </a>
                              </div>
                              <div class="logo_top col-lg-3 col-md-3">
                                 <a href="" class="logo-wrapper ">					
                                 <img src="100/145/391/themes/656787/assets/logo.png?1532588934329" alt="logo ">					
                                 </a>
                              </div>
                              <div class="cart_ico_mobile hidden-lg hidden-md">
                                 <div class="mini-cart-mobile">
                                    <a class="cart_pcs" href="cart"><i class="ion-bag"></i><span class="cartCount  count_item_pr"></span></a>
                                 </div>
                              </div>
                              <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 col-search-engine">
                                 <div class="header_search">
                                    <form class="input-group search-bar" action="search" method="get" role="search">
                                       <div class="collection-selector hidden hidden-xs hidden-sm">
                                          <div class="search_text">Chọn danh mục</div>
                                          <div id="search_info" class="list_search" style="display: none;">
                                             <div class="search_item" data-coll-id="1601791">Bình hoa chúc mừng</div>
                                             <div class="search_item" data-coll-id="1601790">Kệ hoa chúc mừng</div>
                                             <div class="search_item" data-coll-id="1601789">Hộp hoa chúc mừng</div>
                                             <div class="search_item" data-coll-id="1601778">Bó hoa chúc mừng</div>
                                             <div class="search_item" data-coll-id="1601776">Giỏ hoa chúc mừng</div>
                                             <div class="search_item" data-coll-id="1601588">Xu hướng hoa tươi 2018</div>
                                             <div class="search_item" data-coll-id="1601581">Lan Hồ Điệp</div>
                                             <div class="search_item" data-coll-id="1588890">Sản phẩm yêu thích</div>
                                             <div class="search_item" data-coll-id="1565809">Ngày của Mẹ</div>
                                             <div class="search_item" data-coll-id="1563217">Giỏ hoa cảm ơn</div>
                                             <div class="search_item" data-coll-id="1563216">Giỏ hoa yêu thương</div>
                                             <div class="search_item" data-coll-id="1563200">Giỏ hoa chúc mừng</div>
                                             <div class="search_item" data-coll-id="1559648">Bình hoa tươi</div>
                                             <div class="search_item" data-coll-id="1559647">Kệ hoa tươi</div>
                                             <div class="search_item" data-coll-id="1559646">Hộp hoa tươi</div>
                                             <div class="search_item" data-coll-id="1559641">Giỏ hoa tươi</div>
                                             <div class="search_item" data-coll-id="1559640">Bó hoa tươi</div>
                                             <div class="search_item" data-coll-id="1558754">Hoa tươi khuyến mãi</div>
                                             <div class="search_item" data-coll-id="1333193">Hoa cưới</div>
                                             <div class="search_item" data-coll-id="1229950">Hoa chia buồn</div>
                                             <div class="search_item" data-coll-id="1229838">Hoa trang trí - sự kiện</div>
                                             <div class="search_item" data-coll-id="1229836">Hoa yêu thương</div>
                                             <div class="search_item" data-coll-id="1229835">Hoa cảm ơn</div>
                                             <div class="search_item" data-coll-id="1229834">Hoa sinh nhật</div>
                                             <div class="search_item" data-coll-id="1229833">Hoa khai trương</div>
                                             <div class="search_item" data-coll-id="1229832">Hoa chúc mừng</div>
                                             <div class="search_item" data-coll-id="702587">Sản phẩm mới</div>
                                             <div class="liner_search"></div>
                                             <div class="search_item active" data-coll-id="0">Tất cả</div>
                                          </div>
                                       </div>
                                       <input type="search" name="query" value="" placeholder="Bạn muốn tìm sản phẩm gì... " class="input-group-field st-default-search-input search-text" autocomplete="off" required>
                                       <span class="input-group-btn">
                                       <button class="btn icon-fallback-text">
                                       <i class="ion-android-search"></i>
                                       </button>
                                       </span>
                                    </form>
                                 </div>
                              </div>
                              <div class="col-lg-2 col-md-2 col-sm-2 hidden-sm hidden-xs">
                                 <div class="phone_main_menu">
                                    <div class="downdown_cart hidden-sm hidden-xs">
                                       <div class="cart_ico inline-block ico_width">
                                          <div class="mini-cart">
                                             <a class="cart_pcs" href="cart"><i class="ion-bag"></i> &nbsp;<span class="t_line">Giỏ hàng (<span class="cartCount  count_item_pr"></span>)</span></a>
                                          </div>
                                          <div class="top-cart-content">
                                             <ul id="cart-sidebar" class="mini-products-list count_li">
                                                <li class="list-item">
                                                   <ul></ul>
                                                </li>
                                                <li class="action">
                                                   <ul>
                                                      <li class="li-fix-1">
                                                         <div class="top-subtotal">
                                                            Tổng tiền thanh toán: 
                                                            <span class="price"></span>
                                                         </div>
                                                      </li>
                                                      <li class="li-fix-2" style="">
                                                         <div class="actions">
                                                            <a href="cart" class="btn btn-primary">
                                                            <span>Giỏ hàng</span>
                                                            </a>
                                                            <a href="checkout" class="btn btn-checkout btn-gray">
                                                            <span>Thanh toán</span>
                                                            </a>
                                                         </div>
                                                      </li>
                                                   </ul>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="wrap_main">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="wrap_hed">
                           <div class="col-lg-12 col-md-12">
                              <div class="main_nav_header">
                                 <nav class="hidden-sm hidden-xs nav-main">
                                    <div class="menu_hed head_1">
                                       <ul class="nav nav_1">
                                          <li class=" nav-item nav-items  ">
                                             <a class="nav-link" href="">
                                             Trang chủ 
                                             <span class="label_">
                                             </span>
                                             </a>
                                          </li>
                                          <li class="menu_hover nav-item nav-items ">
                                             <a href="collections/all" class="nav-link ">
                                             Sản phẩm <i class="fa fa-caret-down" data-toggle="dropdown"></i>
                                             <span class="label_">
                                             <i class="label new">new</i>
                                             </span>
                                             </a>	
                                             <ul class="dropdown-menu border-box">
                                                <li class="dropdown-submenu nav-items nav-item-lv2">
                                                   <a class="nav-link" href="hoa-chuc-mung">Hoa chúc mừng </a>
                                                   <i class="fa fa-caret-right" data-toggle="dropdown"></i>
                                                   <ul class="dropdown-menu border-box">
                                                      <li class="nav-item-lv3">
                                                         <a class="nav-link" href="gio-hoa-chuc-mung-1">Giỏ hoa</a>
                                                      </li>
                                                      <li class="nav-item-lv3">
                                                         <a class="nav-link" href="bo-hoa-chuc-mung">Bó hoa</a>
                                                      </li>
                                                      <li class="nav-item-lv3">
                                                         <a class="nav-link" href="hop-hoa-chuc-mung">Hộp hoa</a>
                                                      </li>
                                                      <li class="nav-item-lv3">
                                                         <a class="nav-link" href="ke-hoa-chuc-mung">Kệ hoa</a>
                                                      </li>
                                                      <li class="nav-item-lv3">
                                                         <a class="nav-link" href="binh-hoa-chuc-mung">Bình hoa</a>
                                                      </li>
                                                   </ul>
                                                </li>
                                                <li class="nav-item-lv2">
                                                   <a class="nav-link" href="hoa-khai-truong">Hoa khai trương</a>
                                                </li>
                                                <li class="nav-item-lv2">
                                                   <a class="nav-link" href="hoa-sinh-nhat">Hoa sinh nhật</a>
                                                </li>
                                                <li class="nav-item-lv2">
                                                   <a class="nav-link" href="hoa-cam-on">Hoa cảm ơn</a>
                                                </li>
                                                <li class="nav-item-lv2">
                                                   <a class="nav-link" href="hoa-yeu-thuong">Hoa yêu thương</a>
                                                </li>
                                                <li class="nav-item-lv2">
                                                   <a class="nav-link" href="hoa-chia-buon">Hoa chia buồn</a>
                                                </li>
                                                <li class="nav-item-lv2">
                                                   <a class="nav-link" href="hoa-trang-tri-su-kien">Hoa trang trí – sự kiện</a>
                                                </li>
                                                <li class="nav-item-lv2">
                                                   <a class="nav-link" href="hoa-cuoi">Hoa cưới</a>
                                                </li>
                                             </ul>
                                          </li>
                                          <li class=" nav-item nav-items  ">
                                             <a class="nav-link" href="lan-ho-diep">
                                             Lan Hồ Điệp 
                                             <span class="label_">
                                             </span>
                                             </a>
                                          </li>
                                          <li class=" nav-item nav-items  ">
                                             <a class="nav-link" href="blog-tu-van">
                                             Blog tư vấn 
                                             <span class="label_">
                                             </span>
                                             </a>
                                          </li>
                                          <li class=" nav-item nav-items  ">
                                             <a class="nav-link" href="lien-he">
                                             Liên hệ 
                                             <span class="label_">
                                             <i class="label "></i>
                                             </span>
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                 </nav>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <section class="bread-crumb">
            <span class="crumb-border"></span>
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 a-left">
                     <ul class="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <li class="home">
                           <a itemprop="url" href="" ><span itemprop="title">Trang chủ</span></a>					
                           <span class="mr_lr">&nbsp;/&nbsp;</span>
                        </li>
                        <li>
                           <a itemprop="url" href="gio-hoa-chuc-mung-1"><span itemprop="title">Giỏ hoa chúc mừng</span></a>						
                           <span class="mr_lr">&nbsp;/&nbsp;</span>
                        </li>
                        <li><strong><span itemprop="title">NÉT DUYÊN NGẦM</span></strong>
                        <li>					
                     </ul>
                  </div>
               </div>
            </div>
         </section>
         <section class="product margin-top-40" itemscope itemtype="http://schema.org/Product">
            <meta itemprop="url" content="//bonmuahoa.com/net-duyen-ngam">
            <meta itemprop="image" content="thumb/grande/100/145/391/products/bmh-028-jpeg.jpg?v=1529976505177">
            <div class="container">
               <div class="row">
                  <div class="details-product">
                     <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="rows">
                           <form enctype="multipart/form-data" id="add-to-cart-form" action="cart/add" method="post" class="form-inline">
                              <h1 class="title-product" itemprop="name">NÉT DUYÊN NGẦM</h1>
                              <div class="product-detail-left product-images col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                 <div class="leftImg">
                                    <script type='text/javascript' src='js/jquery3-3.js'></script>
                                    <script type='text/javascript' src='js/jquery.elevatezoom.js'></script>
                                    <div class="imgLage">
                                       <img id="zoom_01" src="https://bizweb.dktcdn.net/thumb/1024x1024/100/145/391/products/hdm-023-jpg.jpg?v=1527583422160" data-zoom-image="https://bizweb.dktcdn.net/thumb/1024x1024/100/145/391/products/hdm-023-jpg.jpg?v=1527583422160" />
                                    </div>
                                    <!-- <div id="gal1" class="imgmedum">
                                       @if(!empty($product->image_list))
                                         @foreach(explode(',', $product->image_list) as $imageProduct)   
                                            <a href="#" data-image="{{$imageProduct}}" data-zoom-image="{{$imageProduct}}">
                                           <img id="zoom_01" src="{{$imageProduct}}" />
                                           </a>
                                            
                                          @endforeach
                                       @endif
                                       </div> -->
                                    <div id="gal1" class="imgmedum">
                                       <a href="#" data-image="https://bizweb.dktcdn.net/thumb/1024x1024/100/145/391/products/hdm-023-jpg.jpg?v=1527583422160" data-zoom-image="https://bizweb.dktcdn.net/thumb/1024x1024/100/145/391/products/hdm-023-jpg.jpg?v=1527583422160">
                                       <img id="zoom_01" src="https://bizweb.dktcdn.net/thumb/1024x1024/100/145/391/products/hdm-023-jpg.jpg?v=1527583422160" />
                                       </a>
                                       <a href="#" data-image="https://bizweb.dktcdn.net/thumb/1024x1024/100/145/391/products/lan-ho-diep-hdc-058-min.jpg?v=1527583401143" data-zoom-image="https://bizweb.dktcdn.net/thumb/1024x1024/100/145/391/products/lan-ho-diep-hdc-058-min.jpg?v=1527583401143">
                                       <img id="zoom_01" src="https://bizweb.dktcdn.net/thumb/1024x1024/100/145/391/products/lan-ho-diep-hdc-058-min.jpg?v=1527583401143" />
                                       </a>
                                    </div>
                                    <style type="text/css">
                                       .leftImg .imgLage
                                       {
                                       border: 1px solid #ccc;
                                       text-align: center;
                                       }
                                       .leftImg .imgLage img
                                       {
                                       height: 350px;
                                       max-height: 350px;
                                       }
                                       #gal1
                                       {
                                       text-align: center;
                                       }
                                       #gal1 a img
                                       {
                                       width: 100px;
                                       max-width: 100px;
                                       }
                                    </style>
                                    <script>
                                       //initiate the plugin and pass the id of the div containing gallery images 
                                       $("#zoom_01").elevateZoom({ gallery: 'gal1', cursor: 'pointer' });
                                       //pass the images to Fancybox 
                                       $("#zoom_01").bind("click", function (e) {
                                       var ez = $('#zoom_01').data('elevateZoom');
                                       $.fancybox(ez.getGalleryList()); return false; 
                                       $('#zoom_01').css('width','420px');
                                       $('#zoom_01').css('height','280px');
                                       });
                                        
                                    </script>
                                 </div>
                              </div>
                              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 details-pro">
                                 <div class="thumb_gallary">
                                 </div>
                                 <div class="group-status">
                                    <span class="first_status"><span class="a_name">Thương hiệu</span> <span class="status_name">
                                    Bốn Mùa Hoa
                                    </span>
                                    </span>
                                    <span class="first_status"><span class="a_name">Tình trạng</span> <span class="status_name availabel">
                                    Còn hàng
                                    </span></span>
                                    <span class="first_status"><span class="a_name">Mã sản phẩm</span>
                                    <span class="status_name">
                                    BMH-028
                                    </span>
                                    </span>
                                    <div class="reviews_details_product">
                                       <div class="bizweb-product-reviews-badge" data-id="12020722"></div>
                                       <span class="danhgiasp"><i class="ion-ios-compose-outline"></i><a href="#" onclick="scrollToxx();">&nbsp;Đánh giá sản phẩm này</a></span>
                                    </div>
                                 </div>
                                 <div class="price-box" itemscope itemtype="http://schema.org/Offer">
                                    <div class="special-price">
                                       <span class="price product-price" itemprop="price">370.000 VND</span> 
                                       <meta itemprop="priceCurrency" content="VND">
                                    </div>
                                    <!-- Giá -->
                                 </div>
                                 <div class="freeship">
                                 </div>
                                 <div class="product-summary product_description">
                                    <div class="rte description text3line">
                                       Hồng Trứng gà, Hồng đỏ,... 
                                    </div>
                                 </div>
                                 <div class="form-product col-sm-12">
                                    <div class="box-variant clearfix ">
                                       <input type="hidden" name="variantId" value="19251640" />
                                    </div>
                                    <div class="form-group form_button_details ">
                                       <header class="not_bg">Số lượng:</header>
                                       <div class="custom input_number_product custom-btn-number form-control">									
                                          <button class="btn_num num_1 button button_qty" onClick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro ) &amp;&amp; qtypro &gt; 1 ) result.value--;return false;" type="button">-</button>
                                          <input type="text" id="qtym" name="quantity" value="1"  class="form-control prd_quantity" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" onchange="if(this.value == 0)this.value=1;">
                                          <button class="btn_num num_2 button button_qty" onClick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro )) result.value++;return false;" type="button">+</button>
                                       </div>
                                       <button type="submit" class="btn btn-lg  btn-cart button_cart_buy_enable add_to_cart btn_buy">
                                       <i class="ion-bag"></i>&nbsp;&nbsp;<span>Mua ngay</span>
                                       </button>									
                                    </div>
                                 </div>
                              </div>
                           </form>
                           <!-- Tab -->
                           <div class="tab_width_full">
                              <div class="row margin-top-30 xs-margin-top-15">
                                 <div id="tab_ord" class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                                    <!-- Nav tabs -->
                                    <div class="product-tab e-tabs not-dqtab">
                                       <span class="border-dashed-tab"></span>
                                       <ul class="tabs tabs-title clearfix">
                                          <li class="tab-link" data-tab="tab-1">
                                             <h3><i class="ti-info-alt"></i><span>Mô tả</span></h3>
                                          </li>
                                          <li class="tab-link" data-tab="tab-3" id="tab-review">
                                             <h3><i class="ti-comments"></i><span>Đánh giá</span></h3>
                                          </li>
                                       </ul>
                                       <div class="tab-float">
                                          <div id="tab-1" class="tab-content content_extab">
                                             <div class="rte">
                                                <h2 style="text-align: justify;">GIỎ HOA TƯƠI – NÉT DUYÊN NGẦM</h2>
                                                <p style="text-align: justify;">Với sự kết hợp đơn giản đầy tinh tế của Hồng trứng gà và Hồng đỏ, Giỏ hoa tươi <strong>NÉT DUYÊN NGẦM</strong> gợi lên nét đẹp duyên dáng của người phụ nữ. Những nụ Hồng trứng tròn trịa xếp khít nhau như sự khép nép, e ấp đầy nữ tính mà tạo hóa đã ban tặng riêng cho phái đẹp. Tình yêu mãnh liệt của hoa Hồng đỏ cũng được thể hiện một cách hữu ý ở đây, chúng duyên dáng đến đáng yêu khi lượn một đường mềm mại giữa nền vàng, một chút sắc xanh của Dương Xỉ và lá Chanh điểm xuyết cho giỏ hoa thêm phần sinh động.</p>
                                                <p style="text-align: justify;">Giỏ hoa tươi <strong>NÉT DUYÊN NGẦM </strong>là sự lựa chọn lý tưởng cho các tín đồ yêu thích màu vàng mơ và màu đỏ của hoa Hồng. Không chỉ với thiết kế đẹp, ngay cả hương thơm cũng làm đắm say lòng người.</p>
                                                <p style="text-align: justify;">Kích thước giỏ hoa 10cm x 30cm x 40cm.</p>
                                                <h3 style="text-align: justify;"><strong>Để giữ hoa tươi lâu hơn Quý khách cần lưu ý:</strong></h3>
                                                <p style="text-align: justify;">- Tất cả sản phẩm của Bốn Mùa Hoa thiết kế đều có phần nước dự trữ bên dưới giúp hoa giữ được độ tươi lâu, bạn không cần tháo hoa ra khỏi nơi cắm để giữ nguyên được vẻ đẹp của hoa.</p>
                                                <p style="text-align: justify;">- Nên đặt hoa ở nơi mát mẻ, tránh gió thổi mạnh trực tiếp, không nên đặt hoa nơi có ánh nắng mặt trời chiếu thẳng vào, nơi phát ra nguồn nhiệt gây nóng.</p>
                                                <p style="text-align: justify;">- Để thêm nước cho hoa, bạn chỉ cần châm một ít nước ở phía trên, nước sẽ từ từ chảy xuống phần gốc giúp hoa giữ được vẻ tươi tắn lâu hơn.</p>
                                                <h3 style="text-align: justify;"><strong>Quý khách có thể xem sản phẩm trực tiếp tại:</strong></h3>
                                                <p style="text-align: justify;">- <a href="" style="color: #0aa07d; font-weight: bold">Shop hoa tươi nghệ thuật Bốn Mùa Hoa:</a> Số 1041 Cách Mạng Tháng Tám, Phường 4, Quận Tân Bình, TPHCM. Hoặc đặt mua online tại <a href="http://www.bonmuahoa.com/">www.bonmuahoa.com</a></p>
                                                <p style="text-align: justify;">- Liên hệ ngay Hotline: <a href="tel:0915828039" style="color: #ff0000; font-weight: bold">0915 828 039</a> hoặc số điện thoại: <a href="tel:02862709166" style="color: #ff0000; font-weight: bold">028 6270 9166</a></p>
                                                <p style="text-align: justify;">- Giao hàng tận nơi, thanh toán khi nhận hàng.</p>
                                             </div>
                                          </div>
                                          <div id="tab-3" class="tab-content content_extab tab-review-c">
                                             <div class="rte">
                                                <div id="bizweb-product-reviews" class="bizweb-product-reviews" data-id="12020722">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- Endtab -->
                        </div>
                     </div>
                     <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                        <aside class="aside_product_index margin-bottom-10">
                           <div class="title_title">
                              <h2>
                                 <a href="hoa-tuoi-khuyen-mai" title="Sản phẩm giá tốt">
                                 Sản phẩm giá tốt
                                 </a>
                              </h2>
                           </div>
                           <div class="product_loop_inaside combo margin-top-5">
                              <div class="owl_product_mini">
                                 <div class="item">
                                    <div class="pro-item">
                                       <div class="product-box product-list-small">
                                          <div class="product-thumbnail">
                                             <a href="bo-hoa-tuoi-svh-169" title="CÁT TƯỜNG">
                                             <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/hoa-tuoi-svh-169.jpg?v=1527583379487" alt="CÁT TƯỜNG">
                                             </a>	
                                          </div>
                                          <div class="product-info a-left">
                                             <h3 class="product-name"><a class="ab text1line" href="bo-hoa-tuoi-svh-169" title="CÁT TƯỜNG">CÁT TƯỜNG</a></h3>
                                             <div class="price-box clearfix">
                                                <span class="price product-price">249.000 VND</span>
                                             </div>
                                             <div class="review_star">
                                                <div class="bizweb-product-reviews-badge" data-id="8911052"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="pro-item">
                                       <div class="product-box product-list-small">
                                          <div class="product-thumbnail">
                                             <a href="bo-hoa-tuoi-svh-168" title="TIA NẮNG ẤM ÁP">
                                             <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/hoa-tuoi-svh-168.jpg?v=1527583379647" alt="TIA NẮNG ẤM ÁP">
                                             </a>	
                                          </div>
                                          <div class="product-info a-left">
                                             <h3 class="product-name"><a class="ab text1line" href="bo-hoa-tuoi-svh-168" title="TIA NẮNG ẤM ÁP">TIA NẮNG ẤM ÁP</a></h3>
                                             <div class="price-box clearfix">
                                                <span class="price product-price">149.000 VND</span>
                                             </div>
                                             <div class="review_star">
                                                <div class="bizweb-product-reviews-badge" data-id="8911047"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="pro-item">
                                       <div class="product-box product-list-small">
                                          <div class="product-thumbnail">
                                             <a href="bo-hoa-tuoi-svh-167" title="NIỀM TIN">
                                             <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/hoa-tuoi-svh-167.jpg?v=1527583379813" alt="NIỀM TIN">
                                             </a>	
                                          </div>
                                          <div class="product-info a-left">
                                             <h3 class="product-name"><a class="ab text1line" href="bo-hoa-tuoi-svh-167" title="NIỀM TIN">NIỀM TIN</a></h3>
                                             <div class="price-box clearfix">
                                                <span class="price product-price">249.000 VND</span>
                                             </div>
                                             <div class="review_star">
                                                <div class="bizweb-product-reviews-badge" data-id="8911018"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="pro-item">
                                       <div class="product-box product-list-small">
                                          <div class="product-thumbnail">
                                             <a href="gio-hoa-tuoi-svh-146" title="MẸ LÀ ĐỂ YÊU">
                                             <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/hoa-tuoi-svh-146a.jpg?v=1527583384057" alt="MẸ LÀ ĐỂ YÊU">
                                             </a>	
                                          </div>
                                          <div class="product-info a-left">
                                             <h3 class="product-name"><a class="ab text1line" href="gio-hoa-tuoi-svh-146" title="MẸ LÀ ĐỂ YÊU">MẸ LÀ ĐỂ YÊU</a></h3>
                                             <div class="price-box clearfix">
                                                <span class="price product-price">299.000 VND</span>
                                             </div>
                                             <div class="review_star">
                                                <div class="bizweb-product-reviews-badge" data-id="8803043"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="pro-item">
                                       <div class="product-box product-list-small">
                                          <div class="product-thumbnail">
                                             <a href="bo-hoa-tuoi-svh-139" title="BÓ HOA TƯƠI - SVH 139">
                                             <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/hoa-tuoi-svh-139a-jpg.jpg?v=1527583385683" alt="BÓ HOA TƯƠI - SVH 139">
                                             </a>	
                                          </div>
                                          <div class="product-info a-left">
                                             <h3 class="product-name"><a class="ab text1line" href="bo-hoa-tuoi-svh-139" title="BÓ HOA TƯƠI - SVH 139">BÓ HOA TƯƠI - SVH 139</a></h3>
                                             <div class="price-box clearfix">
                                                <span class="price product-price">249.000 VND</span>
                                             </div>
                                             <div class="review_star">
                                                <div class="bizweb-product-reviews-badge" data-id="8778087"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="pro-item">
                                       <div class="product-box product-list-small">
                                          <div class="product-thumbnail">
                                             <a href="gio-hoa-tuoi-svh-131" title="GIỎ HOA TƯƠI - SVH 131">
                                             <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/hoa-tuoi-svh-131.jpg?v=1527583387363" alt="GIỎ HOA TƯƠI - SVH 131">
                                             </a>	
                                          </div>
                                          <div class="product-info a-left">
                                             <h3 class="product-name"><a class="ab text1line" href="gio-hoa-tuoi-svh-131" title="GIỎ HOA TƯƠI - SVH 131">GIỎ HOA TƯƠI - SVH 131</a></h3>
                                             <div class="price-box clearfix">			
                                                <span class="price product-price">269.000 VND</span>
                                                <span class="price product-price-old">
                                                299.000 VND			
                                                </span>
                                             </div>
                                             <div class="review_star">
                                                <div class="bizweb-product-reviews-badge" data-id="8473790"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="pro-item">
                                       <div class="product-box product-list-small">
                                          <div class="product-thumbnail">
                                             <a href="binh-hoa-tuoi-svh-130" title="BÌNH HOA TƯƠI - SVH 130">
                                             <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/hoa-tuoi-svh-130.jpg?v=1527583387763" alt="BÌNH HOA TƯƠI - SVH 130">
                                             </a>	
                                          </div>
                                          <div class="product-info a-left">
                                             <h3 class="product-name"><a class="ab text1line" href="binh-hoa-tuoi-svh-130" title="BÌNH HOA TƯƠI - SVH 130">BÌNH HOA TƯƠI - SVH 130</a></h3>
                                             <div class="price-box clearfix">			
                                                <span class="price product-price">269.000 VND</span>
                                                <span class="price product-price-old">
                                                299.000 VND			
                                                </span>
                                             </div>
                                             <div class="review_star">
                                                <div class="bizweb-product-reviews-badge" data-id="8473756"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="pro-item">
                                       <div class="product-box product-list-small">
                                          <div class="product-thumbnail">
                                             <a href="binh-hoa-tuoi-svh-129" title="BÌNH HOA TƯƠI - SVH 129">
                                             <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/hoa-tuoi-svh-129.jpg?v=1527583387940" alt="BÌNH HOA TƯƠI - SVH 129">
                                             </a>	
                                          </div>
                                          <div class="product-info a-left">
                                             <h3 class="product-name"><a class="ab text1line" href="binh-hoa-tuoi-svh-129" title="BÌNH HOA TƯƠI - SVH 129">BÌNH HOA TƯƠI - SVH 129</a></h3>
                                             <div class="price-box clearfix">			
                                                <span class="price product-price">269.000 VND</span>
                                                <span class="price product-price-old">
                                                299.000 VND			
                                                </span>
                                             </div>
                                             <div class="review_star">
                                                <div class="bizweb-product-reviews-badge" data-id="8473703"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </aside>
                        <div class="aside-item">
                           <div>
                              <div class="title_title">
                                 <h2>
                                    <a href="blog-tu-van" title="Tin hay cho bạn">
                                    Tin hay cho bạn
                                    </a>
                                 </h2>
                              </div>
                              <div class="list-blogs">
                                 <div class="blog_list_item">
                                    <article class="blog-item blog-item-list ">
                                       <div class="blog-item-thumbnail img1" onclick="window.location.href='2-cach-cam-hoa-tet-dep-lung-linh-danh-cho-gia-dinh-tre-voi-chi-phi-chi-khoang-1-trieu-dong';">
                                          <a href="2-cach-cam-hoa-tet-dep-lung-linh-danh-cho-gia-dinh-tre-voi-chi-phi-chi-khoang-1-trieu-dong">
                                             <picture>
                                                <source media="(max-width: 480px)" srcset="thumb/compact/100/145/391/articles/minimag25-03-1518473903554146987809.jpg?v=1525743996833">
                                                <source media="(min-width: 481px) and (max-width: 767px)" srcset="thumb/compact/100/145/391/articles/minimag25-03-1518473903554146987809.jpg?v=1525743996833">
                                                <source media="(min-width: 768px) and (max-width: 1023px)" srcset="thumb/compact/100/145/391/articles/minimag25-03-1518473903554146987809.jpg?v=1525743996833">
                                                <source media="(min-width: 1024px) and (max-width: 1199px)" srcset="thumb/compact/100/145/391/articles/minimag25-03-1518473903554146987809.jpg?v=1525743996833">
                                                <source media="(min-width: 1200px)" srcset="thumb/compact/100/145/391/articles/minimag25-03-1518473903554146987809.jpg?v=1525743996833">
                                                <img src="100/145/391/articles/minimag25-03-1518473903554146987809.jpg?v=1525743996833" style="max-width:100%;" class="img-responsive" alt="2 cách cắm hoa Tết đẹp lung linh dành cho gia đình trẻ với chi phí chỉ khoảng 1 triệu đồng">
                                             </picture>
                                          </a>
                                       </div>
                                       <div class="ct_list_item">
                                          <h3 class="blog-item-name"><a class="text2line" href="2-cach-cam-hoa-tet-dep-lung-linh-danh-cho-gia-dinh-tre-voi-chi-phi-chi-khoang-1-trieu-dong" title="2 cách cắm hoa Tết đẹp lung linh dành cho gia đình trẻ với chi phí chỉ khoảng 1 triệu đồng">2 cách cắm hoa Tết đẹp lung linh dành cho gia đình trẻ với chi phí chỉ khoảng 1 triệu đồng</a></h3>
                                          <span class="time_post"><i class="ion-clock"></i>08/05/2018</span>
                                       </div>
                                    </article>
                                    <article class="blog-item blog-item-list ">
                                       <div class="blog-item-thumbnail img1" onclick="window.location.href='20-11-khong-can-ra-tiem-vi-da-co-5-cach-goi-hoa-vua-dep-vua-don-gian-nay';">
                                          <a href="20-11-khong-can-ra-tiem-vi-da-co-5-cach-goi-hoa-vua-dep-vua-don-gian-nay">
                                             <picture>
                                                <source media="(max-width: 480px)" srcset="thumb/compact/100/145/391/articles/20151228-ankt-cachcamhoa-20-4-f1bf0-1511088391432.jpg?v=1525743632690">
                                                <source media="(min-width: 481px) and (max-width: 767px)" srcset="thumb/compact/100/145/391/articles/20151228-ankt-cachcamhoa-20-4-f1bf0-1511088391432.jpg?v=1525743632690">
                                                <source media="(min-width: 768px) and (max-width: 1023px)" srcset="thumb/compact/100/145/391/articles/20151228-ankt-cachcamhoa-20-4-f1bf0-1511088391432.jpg?v=1525743632690">
                                                <source media="(min-width: 1024px) and (max-width: 1199px)" srcset="thumb/compact/100/145/391/articles/20151228-ankt-cachcamhoa-20-4-f1bf0-1511088391432.jpg?v=1525743632690">
                                                <source media="(min-width: 1200px)" srcset="thumb/compact/100/145/391/articles/20151228-ankt-cachcamhoa-20-4-f1bf0-1511088391432.jpg?v=1525743632690">
                                                <img src="100/145/391/articles/20151228-ankt-cachcamhoa-20-4-f1bf0-1511088391432.jpg?v=1525743632690" style="max-width:100%;" class="img-responsive" alt="20/11 không cần ra tiệm vì đã có 5 cách gói hoa vừa đẹp vừa đơn giản này">
                                             </picture>
                                          </a>
                                       </div>
                                       <div class="ct_list_item">
                                          <h3 class="blog-item-name"><a class="text2line" href="20-11-khong-can-ra-tiem-vi-da-co-5-cach-goi-hoa-vua-dep-vua-don-gian-nay" title="20/11 không cần ra tiệm vì đã có 5 cách gói hoa vừa đẹp vừa đơn giản này">20/11 không cần ra tiệm vì đã có 5 cách gói hoa vừa đẹp vừa đơn giản này</a></h3>
                                          <span class="time_post"><i class="ion-clock"></i>08/05/2018</span>
                                       </div>
                                    </article>
                                    <article class="blog-item blog-item-list ">
                                       <div class="blog-item-thumbnail img1" onclick="window.location.href='3-cach-cam-hoa-trang-tri-nha-dep-tu-nhung-vat-dung-thuong-ngay';">
                                          <a href="3-cach-cam-hoa-trang-tri-nha-dep-tu-nhung-vat-dung-thuong-ngay">
                                             <picture>
                                                <source media="(max-width: 480px)" srcset="thumb/compact/100/145/391/articles/cam-hoa-3-1501399768913.jpg?v=1525743517510">
                                                <source media="(min-width: 481px) and (max-width: 767px)" srcset="thumb/compact/100/145/391/articles/cam-hoa-3-1501399768913.jpg?v=1525743517510">
                                                <source media="(min-width: 768px) and (max-width: 1023px)" srcset="thumb/compact/100/145/391/articles/cam-hoa-3-1501399768913.jpg?v=1525743517510">
                                                <source media="(min-width: 1024px) and (max-width: 1199px)" srcset="thumb/compact/100/145/391/articles/cam-hoa-3-1501399768913.jpg?v=1525743517510">
                                                <source media="(min-width: 1200px)" srcset="thumb/compact/100/145/391/articles/cam-hoa-3-1501399768913.jpg?v=1525743517510">
                                                <img src="100/145/391/articles/cam-hoa-3-1501399768913.jpg?v=1525743517510" style="max-width:100%;" class="img-responsive" alt="3 cách cắm hoa trang trí nhà đẹp từ những vật dụng thường ngày">
                                             </picture>
                                          </a>
                                       </div>
                                       <div class="ct_list_item">
                                          <h3 class="blog-item-name"><a class="text2line" href="3-cach-cam-hoa-trang-tri-nha-dep-tu-nhung-vat-dung-thuong-ngay" title="3 cách cắm hoa trang trí nhà đẹp từ những vật dụng thường ngày">3 cách cắm hoa trang trí nhà đẹp từ những vật dụng thường ngày</a></h3>
                                          <span class="time_post"><i class="ion-clock"></i>08/05/2018</span>
                                       </div>
                                    </article>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 margin-bottom-30">
                     <div class="section_product_related  not_bf_af margin-top-30">
                        <div class="title_title">
                           <h2>
                              <a href="gio-hoa-chuc-mung-1" title="Sản phẩm cùng loại">
                              Sản phẩm cùng loại
                              </a>
                           </h2>
                        </div>
                        <div class="border_wrap col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                           <div class="owl_product_comback recent_product">
                              <div class="section_two_row row">
                                 <div class="tab-content">
                                    <div class="wrp_list_product  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                       <div class="item">
                                          <div class="product-box product-list-small">
                                             <div class="product-thumbnail">
                                                <a href="vang-son" title="VÀNG SON">
                                                <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/bmh-89-min-jpg.jpg?v=1534501170107" alt="VÀNG SON">
                                                </a>	
                                             </div>
                                             <div class="product-info a-left">
                                                <h3 class="product-name"><a class="ab text1line" href="vang-son" title="VÀNG SON">VÀNG SON</a></h3>
                                                <div class="price-box clearfix">
                                                   <span class="price product-price">570.000 VND</span>
                                                </div>
                                                <div class="review_star">
                                                   <div class="bizweb-product-reviews-badge" data-id="12434804"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wrp_list_product  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                       <div class="item">
                                          <div class="product-box product-list-small">
                                             <div class="product-thumbnail">
                                                <a href="khuc-thu-nong-nan" title="KHÚC THU NỒNG NÀN">
                                                <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/bmh-88-min.jpg?v=1534501109913" alt="KHÚC THU NỒNG NÀN">
                                                </a>	
                                             </div>
                                             <div class="product-info a-left">
                                                <h3 class="product-name"><a class="ab text1line" href="khuc-thu-nong-nan" title="KHÚC THU NỒNG NÀN">KHÚC THU NỒNG NÀN</a></h3>
                                                <div class="price-box clearfix">
                                                   <span class="price product-price">490.000 VND</span>
                                                </div>
                                                <div class="review_star">
                                                   <div class="bizweb-product-reviews-badge" data-id="12434760"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wrp_list_product  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                       <div class="item">
                                          <div class="product-box product-list-small">
                                             <div class="product-thumbnail">
                                                <a href="tho-ngay" title="THƠ NGÂY">
                                                <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/bmh-034-min-jpeg.jpg?v=1529658506060" alt="THƠ NGÂY">
                                                </a>	
                                             </div>
                                             <div class="product-info a-left">
                                                <h3 class="product-name"><a class="ab text1line" href="tho-ngay" title="THƠ NGÂY">THƠ NGÂY</a></h3>
                                                <div class="price-box clearfix">
                                                   <span class="price product-price">380.000 VND</span>
                                                </div>
                                                <div class="review_star">
                                                   <div class="bizweb-product-reviews-badge" data-id="11816844"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wrp_list_product  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                       <div class="item">
                                          <div class="product-box product-list-small">
                                             <div class="product-thumbnail">
                                                <a href="sac-hong-xinh" title="SẮC HỒNG XINH">
                                                <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/bmh-014-min.jpg?v=1529659522980" alt="SẮC HỒNG XINH">
                                                </a>	
                                             </div>
                                             <div class="product-info a-left">
                                                <h3 class="product-name"><a class="ab text1line" href="sac-hong-xinh" title="SẮC HỒNG XINH">SẮC HỒNG XINH</a></h3>
                                                <div class="price-box clearfix">
                                                   <span class="price product-price">520.000 VND</span>
                                                </div>
                                                <div class="review_star">
                                                   <div class="bizweb-product-reviews-badge" data-id="11815509"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wrp_list_product  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                       <div class="item">
                                          <div class="product-box product-list-small">
                                             <div class="product-thumbnail">
                                                <a href="gau-con-may-man" title="GẤU CON MAY MẮN">
                                                <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/bmh-008b-min.jpg?v=1529661304107" alt="GẤU CON MAY MẮN">
                                                </a>	
                                             </div>
                                             <div class="product-info a-left">
                                                <h3 class="product-name"><a class="ab text1line" href="gau-con-may-man" title="GẤU CON MAY MẮN">GẤU CON MAY MẮN</a></h3>
                                                <div class="price-box clearfix">
                                                   <span class="price product-price">480.000 VND</span>
                                                </div>
                                                <div class="review_star">
                                                   <div class="bizweb-product-reviews-badge" data-id="11815327"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wrp_list_product  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                       <div class="item">
                                          <div class="product-box product-list-small">
                                             <div class="product-thumbnail">
                                                <a href="chan-thanh" title="CHÂN THÀNH">
                                                <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/c50f0d67c0872ed97796.jpg?v=1527663985967" alt="CHÂN THÀNH">
                                                </a>	
                                             </div>
                                             <div class="product-info a-left">
                                                <h3 class="product-name"><a class="ab text1line" href="chan-thanh" title="CHÂN THÀNH">CHÂN THÀNH</a></h3>
                                                <div class="price-box clearfix">
                                                   <span class="price product-price">570.000 VND</span>
                                                </div>
                                                <div class="review_star">
                                                   <div class="bizweb-product-reviews-badge" data-id="11815311"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wrp_list_product  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                       <div class="item">
                                          <div class="product-box product-list-small">
                                             <div class="product-thumbnail">
                                                <a href="niem-vui-nhan-doi" title="NIỀM VUI NHÂN ĐÔI">
                                                <img src="thumb/small/100/145/391/themes/656787/assets/ajaxloader.gif?1532588934329" data-lazyload="thumb/small/100/145/391/products/54fbb4957975972bce64.jpg?v=1527662943157" alt="NIỀM VUI NHÂN ĐÔI">
                                                </a>	
                                             </div>
                                             <div class="product-info a-left">
                                                <h3 class="product-name"><a class="ab text1line" href="niem-vui-nhan-doi" title="NIỀM VUI NHÂN ĐÔI">NIỀM VUI NHÂN ĐÔI</a></h3>
                                                <div class="price-box clearfix">
                                                   <span class="price product-price">570.000 VND</span>
                                                </div>
                                                <div class="review_star">
                                                   <div class="bizweb-product-reviews-badge" data-id="11815141"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div id="aweCallback">
            <script>
               var productJson = {"id":12020722,"name":"NÉT DUYÊN NGẦM","alias":"net-duyen-ngam","vendor":"Bốn Mùa Hoa","type":null,"price":370000.0000,"price_max":370000.0000,"price_min":370000.0000,"price_varies":false,"compare_at_price_max":0.0,"compare_at_price_min":0.0,"compare_at_price_varies":false,"template_layout":null,"tags":["Giỏ hoa tươi","Hoa chúc mừng","Hoa yêu thương"],"meta_title":null,"meta_description":null,"summary":"\u003cp style=\u0022text-align: justify;\u0022\u003eHồng Trứng gà, Hồng đỏ,...\u003c/p\u003e","featured_image":{"alt":"GIỎ HOA TƯƠI – NÉT DUYÊN NGẦM","id":27133948,"product_id":12020722,"position":"1","src":"100/145/391/products/bmh-028-jpeg.jpg?v=1529976505177","attached_to_variant":false,"variant_ids":[],"width":4167,"height":5001},"images":[{"alt":"GIỎ HOA TƯƠI – NÉT DUYÊN NGẦM","id":27133948,"product_id":12020722,"position":"1","src":"100/145/391/products/bmh-028-jpeg.jpg?v=1529976505177","attached_to_variant":false,"variant_ids":[],"width":4167,"height":5001}],"options":["Title"],"variants":[{"id":19251640,"barcode":null,"sku":"BMH-028","price":370000.0000,"compare_at_price":null,"options":["Default Title"],"option1":"Default Title","option2":null,"option3":null,"title":"Default Title","taxable":false,"inventory_management":"","inventory_policy":"deny","inventory_quantity":1,"weight":0.0,"weight_unit":"kg","image":null,"requires_shipping":true,"selected":false,"url":"net-duyen-ngam?variantid=19251640","available":true}],"selected_variant":null,"available":true,"content":"\u003ch2 style=\u0022text-align: justify;\u0022\u003eGIỎ HOA TƯƠI – NÉT DUYÊN NGẦM\u003c/h2\u003e\n\u003cp style=\u0022text-align: justify;\u0022\u003eVới sự kết hợp đơn giản đầy tinh tế của Hồng trứng gà và Hồng đỏ, Giỏ hoa tươi \u003cstrong\u003eNÉT DUYÊN NGẦM\u003c/strong\u003e gợi lên nét đẹp duyên dáng của người phụ nữ. Những nụ Hồng trứng tròn trịa xếp khít nhau như sự khép nép, e ấp đầy nữ tính mà tạo hóa đã ban tặng riêng cho phái đẹp. Tình yêu mãnh liệt của hoa Hồng đỏ cũng được thể hiện một cách hữu ý ở đây, chúng duyên dáng đến đáng yêu khi lượn một đường mềm mại giữa nền vàng, một chút sắc xanh của Dương Xỉ và lá Chanh điểm xuyết cho giỏ hoa thêm phần sinh động.\u003c/p\u003e\n\u003cp style=\u0022text-align: justify;\u0022\u003eGiỏ hoa tươi \u003cstrong\u003eNÉT DUYÊN NGẦM \u003c/strong\u003elà sự lựa chọn lý tưởng cho các tín đồ yêu thích màu vàng mơ và màu đỏ của hoa Hồng. Không chỉ với thiết kế đẹp, ngay cả hương thơm cũng làm đắm say lòng người.\u003c/p\u003e\n\u003cp style=\u0022text-align: justify;\u0022\u003eKích thước giỏ hoa 10cm x 30cm x 40cm.\u003c/p\u003e","summary_or_content":"\u003cp style=\u0022text-align: justify;\u0022\u003eHồng Trứng gà, Hồng đỏ,...\u003c/p\u003e","created_on":"2018-06-26T08:27:17Z","object_type":null};
               
               var variantsize = false;
                
                
                var productOptionsSize = 1;
                 var optionsFirst = 'Title';
                 
               
               var cdefault = 1;
                
            </script>
         </div>
         <div class="product-recommend-module-box" style="display: none;">
            <style>
               #owl-product-recommend .item {
               margin: 3px;
               }
               #owl-product-recommend .item img {
               display: block;
               width: 50%;
               height: auto;
               margin: 0 auto;
               }
            </style>
            <link href="Content/styles/css/frontend/module-style.css" rel="stylesheet" />
            <div class="product-recommend-module-title">
            </div>
            <div id="owl-product-recommend" class="">
            </div>
            <script>
               var BizwebProductRecommendApp = BizwebProductRecommendApp || {};
               BizwebProductRecommendApp.productId = "12020722";
            </script>
         </div>
         <link href='100/145/391/themes/656787/assets/bpr-products-module.css?1532588934329' rel='stylesheet' type='text/css' />
         <div class="bizweb-product-reviews-module"></div>
         <footer id="footer">
            <div class="topfoter">
               <div class="container">
                  <div class="top-footer">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 footer-click">
                           <h4 class="clikedfalse margin-bottom-35">Shop Hoa tươi Nghệ thuật Bốn Mùa Hoa</h4>
                           <ul class="ct_footer">
                              <li>
                                 <span class="icon_left"><i class="ion-location"></i></span>
                                 <span class="content_r">1041 Cách Mạng Tháng Tám, Phường 4, Quận Tân Bình, TPHCM, Việt Nam</span>
                              </li>
                              <li class="not-before">
                                 <span class="icon_left"><i class="ion-android-call"></i></span> 
                                 <span class="content_r">
                                 Điện thoại: <a href="tel:02862709166">028 6270 9166 </a>
                                 </span>
                              </li>
                              <li class="not-before">
                                 <span class="icon_left"><i class="ion-android-call"></i></span> 
                                 <span class="content_r">
                                 Hotline: <a href="tel:0915828039">0915 828 039 </a>
                                 </span>
                              </li>
                              <li class="not-before" >
                                 <span class="icon_left"><i class="ion-android-mail"></i></span>
                                 <span class="content_r">
                                 Email: <a href="mailto:bonmuahoa.hcm@gmail.com">bonmuahoa.hcm@gmail.com</a>
                                 </span>
                              </li>
                           </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer-click">
                           <h4 class="cliked">Về chúng tôi</h4>
                           <ul class="toggle-mn" style="display:none;">
                              <li><a class="ef" href="ve-chung-toi">Giới thiệu</a></li>
                              <li><a class="ef" href="trach-nhiem-xa-hoi">Trách nhiệm xã hội</a></li>
                              <li><a class="ef" href="chinh-sach-chat-luong">Chính sách chất lượng</a></li>
                              <li><a class="ef" href="chinh-sach-bao-mat-1">Chính sách bảo mật</a></li>
                           </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer-click">
                           <h4 class="cliked">Khách hàng</h4>
                           <ul class="toggle-mn" style="display:none;">
                              <li><a class="ef" href="huo-ng-da-n-mua-hang">Hướng dẫn mua hàng</a></li>
                              <li><a class="ef" href="huong-dan-thanh-toan">Đặt hàng và thanh toán</a></li>
                              <li><a class="ef" href="chinh-sach-doi-tra-hang">Chinh sách đổi trả hàng</a></li>
                              <li><a class="ef" href="chinh-sach-van-chuyen">Chính sách vận chuyển</a></li>
                              <li><a class="ef" href="die-u-khoa-n-su-dung">Điều khoản sử dụng</a></li>
                           </ul>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 footer-click">
                           <h4 class="cliked">Đào tạo</h4>
                           <ul class="toggle-mn" style="display:none;">
                              <li><a class="ef" href="lop-hoc-cam-hoa-can-ban">Lớp học cắm hoa căn bản</a></li>
                              <li><a class="ef" href="">Lớp học cắm hoa chuyên nghiệp</a></li>
                              <li><a class="ef" href="">Lớp học cắm hoa theo yêu cầu</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="bottom-footer">
               <div class="container">
                  <div class="row row_footer">
                     <div id="copy1" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div class="row tablet">
                           <div id="copyright" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fot_copyright a-center">
                              <span class="wsp"><span class="mobile">@2018 - Bản quyền thuộc về Bốn Mùa Hoa </span><span class="hidden-xs"> | </span><span class="mobile">Cung cấp bởi <a rel="nofollow" href="https://www.sapo.vn" title="Sapo" target="_blank">Sapo</a></span></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <a href="#" id="back-to-top" class="backtop"  title="Lên đầu trang"><i class="fa fa-angle-up"></i></a>
         </footer>
         <div class="support-online">
            <div class="support-content" style="display: none;">
               <a href="tel:0915828039" class="call-now" rel="nofollow">
                  <i class="fa fa-whatsapp" aria-hidden="true"></i>
                  <div class="animated infinite zoomIn kenit-alo-circle"></div>
                  <div class="animated infinite pulse kenit-alo-circle-fill"></div>
                  <span>Gọi ngay: 0915 828039</span>
               </a>
               <a class="mes" href="https://m.me/bonmuahoa.hcm">
               <i class="fa fa-facebook-official" aria-hidden="true"></i>
               <span>Nhắn tin facebook</span>
               </a>
               <a class="zalo" href="http://zalo.me/0915 828039">
               <i class="fa ion-chatbubble-working"></i>
               <span>Zalo: 0915 828039</span>
               </a>
               <a class="sms" href="sms:0915 828039">
               <i class="fa fa-weixin" aria-hidden="true"></i>
               <span>SMS: 0915 828039</span>
               </a>
            </div>
            <a class="btn-support">
               <i class="fa ion-android-contact" aria-hidden="true"></i>
               <div class="animated infinite zoomIn kenit-alo-circle"></div>
               <div class="animated infinite pulse kenit-alo-circle-fill"></div>
            </a>
         </div>
         <script src='100/145/391/themes/656787/assets/jquery-2.2.3.min.js?1532588934329' type='text/javascript'></script> 
         <script>			</script>
         <!-- Bizweb javascript -->
         <script src='100/145/391/themes/656787/assets/option-selectors.js?1532588934329' type='text/javascript'></script>
         <script src='assets/themes_support/api.jquery.js?4' type='text/javascript'></script> 
         <!-- Bizweb javascript customer -->
         <!-- Add to cart -->	
         <div class="ajax-load">
            <span class="loading-icon">
               <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                  <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
                     <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
                  </rect>
                  <rect x="8" y="10" width="4" height="10" fill="#333"  opacity="0.2">
                     <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                  </rect>
                  <rect x="16" y="10" width="4" height="10" fill="#333"  opacity="0.2">
                     <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                  </rect>
               </svg>
            </span>
         </div>
         <div class="loading awe-popup">
            <div class="overlay"></div>
            <div class="loader" title="2">
               <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                  <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
                     <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
                  </rect>
                  <rect x="8" y="10" width="4" height="10" fill="#333"  opacity="0.2">
                     <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                  </rect>
                  <rect x="16" y="10" width="4" height="10" fill="#333"  opacity="0.2">
                     <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                     <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                  </rect>
               </svg>
            </div>
         </div>
         <div class="addcart-popup product-popup awe-popup">
            <div class="overlay no-background"></div>
            <div class="content">
               <div class="row row-noGutter">
                  <div class="col-xl-6 col-xs-12">
                     <div class="btn btn-full btn-primary a-left popup-title"><i class="fa fa-check"></i>Thêm vào giỏ hàng thành công
                     </div>
                     <a href="javascript:void(0)" class="close-window close-popup"><i class="fa fa-close"></i></a>
                     <div class="info clearfix">
                        <div class="product-image margin-top-5">
                           <img alt="popup" src="100/145/391/themes/656787/assets/logo.png?1532588934329" style="max-width:150px; height:auto">
                        </div>
                        <div class="product-info">
                           <p class="product-name"></p>
                           <p class="quantity color-main"><span>Số lượng: </span></p>
                           <p class="total-money color-main"><span>Tổng tiền: </span></p>
                        </div>
                        <div class="actions">    
                           <button class="btn  btn-primary  margin-top-5 btn-continue">Tiếp tục mua hàng</button>        
                           <button class="btn btn-gray margin-top-5" onclick="window.location='cart'">Kiểm tra giỏ hàng</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="error-popup awe-popup">
            <div class="overlay no-background"></div>
            <div class="popup-inner content">
               <div class="error-message"></div>
            </div>
         </div>
         <script>
            Bizweb.updateCartFromForm = function(cart, cart_summary_id, cart_count_id) {
            	if ((typeof cart_summary_id) === 'string') {
            		var cart_summary = jQuery(cart_summary_id);
            		if (cart_summary.length) {
            			// Start from scratch.
            			cart_summary.empty();
            			// Pull it all out.        
            			jQuery.each(cart, function(key, value) {
            				if (key === 'items') {
            
            					var table = jQuery(cart_summary_id);           
            					if (value.length) {   
            						jQuery('<ul class="list-item-cart"></ul>').appendTo(table);
            						jQuery.each(value, function(i, item) {	
            
            							var src = item.image;
            							if(src == null){
            								src = "http:/thumb/large/assets/themes_support/noimage.gif";
            							}
            							var buttonQty = "";
            							if(item.quantity == '1'){
            								// buttonQty = 'disabled';
            							}else{
            								buttonQty = '';
            							}
            							jQuery('<li class="item productid-' + item.variant_id +'"><div class="wrap_item"><a class="product-image" href="' + item.url + '" title="' + item.name + '">'
            								   + '<img alt="'+  item.name  + '" src="' + src +  '"width="'+ '80' +'"\></a>'
            								   + '<div class="detail-item"><div class="product-details"> <a href="javascript:;" data-id="'+ item.variant_id +'" title="Xóa" class="remove-item-cart ti-close">&nbsp;</a>'
            								   + '<h3 class="product-name text1line"> <a href="' + item.url + '" title="' + item.name + '">' + item.name + '</a></h3></div>'
            								   + '<div class="product-details-bottom"><span class="price">' + Bizweb.formatMoney(item.price, "{{amount_no_decimals_with_comma_separator}} VND") + '</span><span class="hidden quaty item_quanty_count"> x '+ item.quantity +'</span>'
            								   + '<div class="quantity-select qty_drop_cart hidden"><input class="variantID" type="hidden" name="variantId" value="'+ item.variant_id +'"><button onClick="var result = document.getElementById(\'qty'+ item.variant_id +'\'); var qty'+ item.variant_id +' = result.value; if( !isNaN( qty'+ item.variant_id +' ) &amp;&amp; qty'+ item.variant_id +' &gt; 1 ) result.value--;return false;" class="btn_reduced reduced items-count btn-minus" ' + buttonQty + ' type="button">–</button><input type="text" maxlength="12" readonly class="input-text number-sidebar qty'+ item.variant_id +'" id="qty'+ item.variant_id +'" name="Lines" id="updates_'+ item.variant_id +'" size="4" value="'+ item.quantity +'"><button onClick="var result = document.getElementById(\'qty'+ item.variant_id +'\'); var qty'+ item.variant_id +' = result.value; if( !isNaN( qty'+ item.variant_id +' )) result.value++;return false;" class="btn_increase increase items-count btn-plus" type="button">+</button></div>'
            								   + '</div></div></li>').appendTo(table.children('.list-item-cart'));
            						}); 
            						jQuery('<div class="wrap_total"><div class="top-subtotal hidden">Phí vận chuyển: <span class="pricex">Tính khi thanh toán</span></div><div class="top-subtotal">Tổng tiền: <span class="price">' + Bizweb.formatMoney(cart.total_price, "{{amount_no_decimals_with_comma_separator}} VND") + '</span></div></div>').appendTo(table);
            						jQuery('<div class="wrap_button"><div class="actions"><a href="cart" class="btn btn-gray btn-cart-page pink"><span>Đến giỏ hàng</span></a> <a href="checkout" class="btn btn-gray btn-checkout pink"><span>Thanh toán</span></a> </div></div>').appendTo(table);
            					}
            					else {
            						jQuery('<div class="no-item"><p>Không có sản phẩm nào.</p></div>').appendTo(table);
            
            					}
            				}
            			});
            		}
            	}
            	updateCartDesc(cart);
            	var numInput = document.querySelector('#cart-sidebar .qty_drop_cart input.input-text');
            	if (numInput != null){
            		// Listen for input event on numInput.
            		numInput.addEventListener('input', function(){
            			// Let's match only digits.
            			var num = this.value.match(/^\d+$/);
            			if (num == 0) {
            				// If we have no match, value will be empty.
            				this.value = 1;
            			}
            			if (num === null) {
            				// If we have no match, value will be empty.
            				this.value = "1";
            			}
            		}, false)
            	}
            }
            
            Bizweb.updateCartPageForm = function(cart, cart_summary_id, cart_count_id) {
            	if ((typeof cart_summary_id) === 'string') {
            		var cart_summary = jQuery(cart_summary_id);
            		if (cart_summary.length) {
            			// Start from scratch.
            			cart_summary.empty();
            			// Pull it all out.        
            			jQuery.each(cart, function(key, value) {
            				if (key === 'items') {
            					var table = jQuery(cart_summary_id);           
            					if (value.length) {  
            
            						var pageCart = '<div class="cart page_cart hidden-xs">'
            						+ '<form action="cart" method="post" novalidate class="margin-bottom-0"><div class="bg-scroll"><div class="cart-thead">'
            						+ '<div style="width: 18%" class="a-center">Ảnh sản phẩm</div><div style="width: 32%" class="a-center">Tên sản phẩm</div><div style="width: 17%" class="a-center"><span class="nobr">Đơn giá</span></div><div style="width: 14%" class="a-center">Số lượng</div><div style="width: 14%" class="a-center">Thành tiền</div><div style="width: 5%" class="a-center">Xoá</div></div>'
            						+ '<div class="cart-tbody"></div></div></form></div>'; 
            						var pageCartCheckout = '<div class="row margin-top-20  margin-bottom-40"><div class="col-lg-7 col-md-7"><div class="form-cart-button"><div class=""><a href="" class="form-cart-continue">Tiếp tục mua hàng</a></div></div></div>'
            						+ '<div class="col-lg-5 col-md-5 bg_cart shopping-cart-table-total"><div class="table-total"><table class="table ">'
            						+ '<tr class="hidden"><td>Tổng giá sản phẩm</td><td class="txt-right totals_price a-right">' + Bizweb.formatMoney(cart.total_price, "{{amount_no_decimals_with_comma_separator}} VND") + '</td></tr>'
            						+ '<tr class="hidden"><td>Tiền vận chuyển</td><td class="txt-right a-right">Tính khi thanh toán</td></tr>'
            						+ '<tr><td class="total-text">Tổng tiền thanh toán</td><td class="txt-right totals_price price_end a-right">' + Bizweb.formatMoney(cart.total_price, "{{amount_no_decimals_with_comma_separator}} VND") + '</td></tr></table></div>'
            						+ '<a onclick="window.location.href=\'checkout\'" class="btn-checkout-cart">Tiến hành thanh toán</a></div></div>';
            						jQuery(pageCart).appendTo(table);
            						jQuery.each(value, function(i, item) {
            							var buttonQty = "";
            							if(item.quantity == '1'){
            								buttonQty = 'disabled';
            							}else{
            								buttonQty = '';
            							}
            							var link_img1 = Bizweb.resizeImage(item.image, 'compact');
            							if(link_img1=="null" || link_img1 =='' || link_img1 ==null){
            								link_img1 = 'https:/thumb/large/assets/themes_support/noimage.gif';
            							}
            							var pageCartItem = '<div class="item-cart productid-' + item.variant_id +'"><div style="width: 18%" class="image"><a class="product-image" title="' + item.name + '" href="' + item.url + '"><img width="75" height="auto" alt="' + item.name + '" src="' + link_img1 +  '"></a></div>'
            							+ '<div style="width: 32%" class="a-center"><h3 class="product-name"> <a class="text2line" href="' + item.url + '">' + item.title + '</a> </h3><span class="variant-title">' + item.variant_title + '</span>'
            							+ '</div><div style="width: 17%" class="a-center"><span class="item-price"> <span class="price">' + Bizweb.formatMoney(item.price, "{{amount_no_decimals_with_comma_separator}} VND") + '</span></span></div>'
            							+ '<div style="width: 14%" class="a-center"><div class="input_qty_pr"><input class="variantID" type="hidden" name="variantId" value="'+ item.variant_id +'">'
            							+ '<input type="text" maxlength="12" readonly min="0" class="check_number_here input-text number-sidebar input_pop input_pop qtyItem'+ item.variant_id +'" id="qtyItem'+ item.variant_id +'" name="Lines" id="updates_'+ item.variant_id +'" size="4" value="'+ item.quantity +'">'
            							+ '<button onClick="var result = document.getElementById(\'qtyItem'+ item.variant_id +'\'); var qtyItem'+ item.variant_id +' = result.value; if( !isNaN( qtyItem'+ item.variant_id +' )) result.value++;return false;" class="increase_pop items-count btn-plus" type="button">+</button><button onClick="var result = document.getElementById(\'qtyItem'+ item.variant_id +'\'); var qtyItem'+ item.variant_id +' = result.value; if( !isNaN( qtyItem'+ item.variant_id +' ) &amp;&amp; qtyItem'+ item.variant_id +' &gt; 1 ) result.value--;return false;" ' + buttonQty + ' class="reduced_pop items-count btn-minus" type="button">-</button></div></div>'
            							+ '<div style="width: 14%" class="a-center"><span class="cart-price"> <span class="price">'+ Bizweb.formatMoney(item.price * item.quantity, "{{amount_no_decimals_with_comma_separator}} VND") +'</span> </span></div>'
            							+ '<div style="width: 5%" class="a-center">'
            							+ '<a class="remove-itemx remove-item-cart" title="Xóa" href="javascript:;" data-id="'+ item.variant_id +'"><span><i class="fa fa-trash-o"></i></span></a>'
            							+'</div>'
            							+ '</div>';
            							jQuery(pageCartItem).appendTo(table.find('.cart-tbody'));
            							if(item.variant_title == 'Default Title'){
            								$('.variant-title').hide();
            							}
            						}); 
            						jQuery(pageCartCheckout).appendTo(table.children('.cart'));
            					}else {
            						jQuery('<p class="hidden-xs-down ">Không có sản phẩm nào. Quay lại <a href="collections/all" style="color:;">cửa hàng</a> để tiếp tục mua sắm.</p>').appendTo(table);
            						jQuery('.cart_desktop_page').css('min-height', 'auto');
            					}
            				}
            			});
            		}
            	}
            	updateCartDesc(cart);
            	jQuery('#wait').hide();
            	
            }
            Bizweb.updateCartPopupForm = function(cart, cart_summary_id, cart_count_id) {
            
            	if ((typeof cart_summary_id) === 'string') {
            		var cart_summary = jQuery(cart_summary_id);
            		if (cart_summary.length) {
            			// Start from scratch.
            			cart_summary.empty();
            			// Pull it all out.        
            			jQuery.each(cart, function(key, value) {
            				if (key === 'items') {
            					var table = jQuery(cart_summary_id);           
            					if (value.length) { 
            						jQuery.each(value, function(i, item) {
            							var src = item.image;
            							if(src == null){
            								src = "http:/thumb/large/assets/themes_support/noimage.gif";
            							}
            							var buttonQty = "";
            							if(item.quantity == '1'){
            								buttonQty = 'disabled';
            							}else{
            								buttonQty = '';
            							}
            							var pageCartItem = '<div class="item-popup productid-' + item.variant_id +'">'
            							+ '<div style="width: 15%;" class="border height image_ text-left"><div class="item-image">'
            							+ '<a class="product-image" href="' + item.url + '" title="' + item.name + '"><img alt="'+  item.name  + '" src="' + src +  '"width="'+ '90' +'"\></a>'
            							+ '</div></div>'
            							+ '<div style="width:38.8%;" class="height text-left"><div class="item-info"><p class="item-name"><a class="text2line" href="' + item.url + '" title="' + item.name + '">' + item.title + '</a></p>'
            							+ '<span class="variant-title-popup">' + item.variant_title + '</span>'
            							+ '<a href="javascript:;" class="remove-item-cart" title="Xóa" data-id="'+ item.variant_id +'"><i class="fa fa-close"></i>&nbsp;&nbsp;Xoá</a>'
            							+ '<p class="addpass" style="color:#fff;margin:0px;">'+ item.variant_id +'</p>'
            							+ '</div></div>'
            							+ '<div style="width: 15.2%;" class="border height text-center"><div class="item-price"><span class="price">' + Bizweb.formatMoney(item.price, "{{amount_no_decimals_with_comma_separator}} VND") + '</span>'
            							+ '</div></div><div style="width: 15.4%;" class="border height text-center"><div class="qty_thuongdq check_"><input class="variantID" type="hidden" name="variantId" value="'+ item.variant_id +'">'
            							+ '<button onClick="var result = document.getElementById(\'qtyItemP'+ item.variant_id +'\'); var qtyItemP'+ item.variant_id +' = result.value; if( !isNaN( qtyItemP'+ item.variant_id +' ) &amp;&amp; qtyItemP'+ item.variant_id +' &gt; 1 ) result.value--;return false;" ' + buttonQty + ' class="num1 reduced items-count btn-minus" type="button">-</button>'
            							+ '<input type="text" maxlength="12" min="0" readonly class="input-text number-sidebar qtyItemP'+ item.variant_id +'" id="qtyItemP'+ item.variant_id +'" name="Lines" id="updates_'+ item.variant_id +'" size="4" value="'+ item.quantity +'">'
            							+ '<button onClick="var result = document.getElementById(\'qtyItemP'+ item.variant_id +'\'); var qtyItemP'+ item.variant_id +' = result.value; if( !isNaN( qtyItemP'+ item.variant_id +' )) result.value++;return false;" class="num2 increase items-count btn-plus" type="button">+</button></div></div>'
            							+ '<div style="width: 15%;" class="border height text-center"><span class="cart-price"> <span class="price">'+ Bizweb.formatMoney(item.price * item.quantity, "{{amount_no_decimals_with_comma_separator}} VND") +'</span> </span></div>'
            							+ '</div>';
            							jQuery(pageCartItem).appendTo(table);
            							if(item.variant_title == 'Default Title'){
            								$('.variant-title-popup').hide();
            							}
            							$('.link_product').text();
            						}); 
            					}
            				}
            			});
            		}
            	}
            	jQuery('.total-price').html(Bizweb.formatMoney(cart.total_price, "{{amount_no_decimals_with_comma_separator}} VND"));
            	
            	updateCartDesc(cart);
            
            }
            Bizweb.updateCartPageFormMobile = function(cart, cart_summary_id, cart_count_id) {
            	if ((typeof cart_summary_id) === 'string') {
            		var cart_summary = jQuery(cart_summary_id);
            		if (cart_summary.length) {
            			// Start from scratch.
            			cart_summary.empty();
            			// Pull it all out.        
            			jQuery.each(cart, function(key, value) {
            				if (key === 'items') {
            
            					var table = jQuery(cart_summary_id);           
            					if (value.length) {   
            						jQuery('<div class="cart_page_mobile content-product-list"></div>').appendTo(table);
            						jQuery.each(value, function(i, item) {
            							if( item.image != null){
            								var src = Bizweb.resizeImage(item.image, 'small');
            							}else{
            								var src = "https:/thumb/large/assets/themes_support/noimage.gif";
            							}
            							jQuery('<div class="item-product item-mobile-cart item productid-' + item.variant_id +' "><div class="item-product-cart-mobile"><a href="' + item.url + '">	<a class="product-images1" href="' + item.url + '"  title="' + item.name + '"><img width="80" height="150" alt="' + item.name + '" src="' + src +  '" alt="' + item.name + '"></a></a></div>'
            								   + '<div class="title-product-cart-mobile"><h3><a class="text2line" href="' + item.url + '" title="' + item.name + '">' + item.name + '</a></h3><p>Giá: <span>' + Bizweb.formatMoney(item.price, "{{amount_no_decimals_with_comma_separator}} VND") + '</span></p></div>'
            								   + '<div class="select-item-qty-mobile"><div class="txt_center in_put check_">'
            								   + '<input class="variantID" type="hidden" name="variantId" value="'+ item.variant_id +'"><button onClick="var result = document.getElementById(\'qtyMobile'+ item.variant_id +'\'); var qtyMobile'+ item.variant_id +' = result.value; if( !isNaN( qtyMobile'+ item.variant_id +' ) &amp;&amp; qtyMobile'+ item.variant_id +' &gt; 0 ) result.value--;return false;" class="reduced items-count btn-minus" type="button">–</button><input type="number" maxlength="12" min="1" readonly class="check_number_here input-text mobile_input number-sidebar qtyMobile'+ item.variant_id +'" id="qtyMobile'+ item.variant_id +'" name="Lines" id="updates_'+ item.variant_id +'" size="4" value="'+ item.quantity +'"><button onClick="var result = document.getElementById(\'qtyMobile'+ item.variant_id +'\'); var qtyMobile'+ item.variant_id +' = result.value; if( !isNaN( qtyMobile'+ item.variant_id +' )) result.value++;return false;" class="increase items-count btn-plus" type="button">+</button></div>'
            								   + '<a class="button remove-item remove-item-cart" href="javascript:;" data-id="'+ item.variant_id +'">Xoá</a></div>').appendTo(table.children('.content-product-list'));
            
            						});
            
            						jQuery('<div class="header-cart-price" style=""><div class="title-cart a-center"><span class="total_mobile a-center">Tổng tiền: <span class=" totals_price_mobile">' + Bizweb.formatMoney(cart.total_price, "{{amount_no_decimals_with_comma_separator}} VND") + '</span><span></div>'
            							   + '<div class="checkout"><button class="btn-proceed-checkout-mobile" title="Tiến hành thanh toán" type="button" onclick="window.location.href=\'checkout\'">'
            							   + '<span>Tiến hành thanh toán</span></button>'
            							   + '<button class="btn btn-white contin" title="Tiếp tục mua hàng" type="button" onclick="window.location.href=\'collections/all\'"><span>Tiếp tục mua hàng</span></button>'
            							   + '</div></div>').appendTo(table);
            					}else {
            						jQuery('<p class="hidden-xs-down col-xs-12">Không có sản phẩm nào. Quay lại <a href="collections/all" style="color:;">cửa hàng</a> để tiếp tục mua sắm.</p>').appendTo(table);
            						jQuery('.cart_desktop_page').css('min-height', 'auto');
            					}
            
            				}
            			});
            		}
            	}
            	
            	updateCartDesc(cart);
            	
            
            }
            
            
            
            function updateCartDesc(data){
            	var $cartPrice = Bizweb.formatMoney(data.total_price, "{{amount_no_decimals_with_comma_separator}} VND"),
            		$cartMobile = $('#header .cart-mobile .quantity-product'),
            		$cartDesktop = $('.count_item_pr'),
            		$cartDesktopList = $('.cart-counter-list'),
            		$cartPopup = $('.cart-popup-count');
            
            	switch(data.item_count){
            		case 0:
            			$cartMobile.text('0');
            			$cartDesktop.text('0');
            			$cartDesktopList.text('0');
            			$cartPopup.text('0');
            
            			break;
            		case 1:
            			$cartMobile.text('1');
            			$cartDesktop.text('1');
            			$cartDesktopList.text('1');
            			$cartPopup.text('1');
            
            			break;
            		default:
            			$cartMobile.text(data.item_count);
            			$cartDesktop.text(data.item_count);
            			$cartDesktopList.text(data.item_count);
            			$cartPopup.text(data.item_count);
            
            			break;
            	}
            	$('.top-cart-content .top-subtotal .price, aside.sidebar .block-cart .subtotal .price, .popup-total .total-price').html($cartPrice);
            	$('.popup-total .total-price').html($cartPrice);
            	$('.shopping-cart-table-total .totals_price').html($cartPrice);
            	$('.header-cart-price .totals_price_mobile').html($cartPrice);
            	$('.cartCount').html(data.item_count);
            }
            
            Bizweb.onCartUpdate = function(cart) {
            	Bizweb.updateCartFromForm(cart, '.mini-products-list');
            	Bizweb.updateCartPopupForm(cart, '#popup-cart-desktop .tbody-popup');
            	
            	 };
            	 Bizweb.onCartUpdateClick = function(cart, variantId) {
            		 jQuery.each(cart, function(key, value) {
            			 if (key === 'items') {    
            				 jQuery.each(value, function(i, item) {	
            					 if(item.variant_id == variantId){
            						 $('.productid-'+variantId).find('.cart-price span.price').html(Bizweb.formatMoney(item.price * item.quantity, "{{amount_no_decimals_with_comma_separator}} VND"));
            						 $('.productid-'+variantId).find('.items-count').prop("disabled", false);
            						 $('.productid-'+variantId).find('.number-sidebar').prop("disabled", false);
            						 $('.productid-'+variantId +' .number-sidebar').val(item.quantity);
            						 $('.productid-'+variantId +' .item_quanty_count').text(item.quantity);
            						 if(item.quantity == '1'){
            							 // $('.productid-'+variantId).find('.items-count.btn-minus').prop("disabled", true);
            						 }
            					 }
            				 }); 
            			 }
            		 });
            		 updateCartDesc(cart);
            	 }
            	 Bizweb.onCartRemoveClick = function(cart, variantId) {
            		 jQuery.each(cart, function(key, value) {
            			 if (key === 'items') {    
            				 jQuery.each(value, function(i, item) {	
            					 if(item.variant_id == variantId){
            						 $('.productid-'+variantId).remove();
            					 }
            				 }); 
            			 }
            		 });
            		 updateCartDesc(cart);
            	 }
            	 $(window).ready(function(){
            		 $.ajax({
            			 type: 'GET',
            			 url: 'cart.js',
            			 async: false,
            			 cache: false,
            			 dataType: 'json',
            			 success: function (cart){
            				 Bizweb.updateCartFromForm(cart, '.mini-products-list');
            				 Bizweb.updateCartPopupForm(cart, '#popup-cart-desktop .tbody-popup'); 
            				 
            				  }
            				 });
            			 });
            
         </script>		
         <div id="popup-cart" class="modal fade" role="dialog">
            <div id="popup-cart-desktop" class="clearfix">
               <div class="title-popup-cart">
                  <img src="100/145/391/themes/656787/assets/check_popup.png?1532588934329"  alt="Bốn Mùa Hoa"> <span class="your_product">Bạn đã thêm <span class="cart-popup-name"></span> vào giỏ hàng thành công ! </span>
               </div>
               <div class="wrap_popup">
                  <div class="title-quantity-popup" >
                     <span class="cart_status" onclick="window.location.href='cart';">Giỏ hàng của bạn có <span class="cart-popup-count"></span> sản phẩm </span>
                  </div>
                  <div class="content-popup-cart">
                     <div class="thead-popup">
                        <div style="width: 53%;" class="text-left">Sản phẩm</div>
                        <div style="width: 15%;" class="text-center">Đơn giá</div>
                        <div style="width: 15%;" class="text-center">Số lượng</div>
                        <div style="width: 17%;" class="text-center">Thành tiền</div>
                     </div>
                     <div class="tbody-popup scrollbar-dynamic">
                     </div>
                     <div class="tfoot-popup">
                        <div class="tfoot-popup-1 a-right clearfix">
                           <span class="total-p popup-total">Tổng tiền thanh toán: <span class="total-price"></span></span>
                        </div>
                        <div class="tfoot-popup-2 clearfix">
                           <a class="button buy_ btn-proceed-checkout" title="Tới giỏ hàng" href="cart"><span><span>Tới giỏ hàng</span></span></a>
                           <a class="button checkout_ btn-proceed-checkout" title="Thanh toán ngay" href="checkout"><span>Thanh toán ngay</span></a>
                        </div>
                     </div>
                  </div>
                  <a title="Close" class="quickview-close close-window" href="javascript:;" onclick="$('#popup-cart').modal('hide');"><i class="fa  fa-close"></i></a>
               </div>
            </div>
         </div>
         <div id="myModal" class="modal fade" role="dialog"></div>
         <script>
            if(navigator.userAgent.indexOf("Speed Insights") == -1) {
            	$.getScript("100/145/391/themes/656787/assets/cs.script.js?1532588934329");
            }
         </script>
         <!-- Main JS -->	
         <script type="text/javascript" src="/s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58589c2252fc2da4"></script>
         <!-- Plugin JS -->
         <!-- Product detail JS,CSS -->
         <link href='100/145/391/themes/656787/assets/lightbox.css?1532588934329' rel='stylesheet' type='text/css' />
         <script src='100/145/391/themes/656787/assets/jquery.elevatezoom308.min.js?1532588934329' type='text/javascript'></script>		
         <script src='100/145/391/themes/656787/assets/jquery.prettyphoto.min005e.js?1532588934329' type='text/javascript'></script>
         <script src='100/145/391/themes/656787/assets/jquery.prettyphoto.init.min367a.js?1532588934329' type='text/javascript'></script>
         <script>
            if(navigator.userAgent.indexOf("Speed Insights") == -1) {
            	$.getScript("100/145/391/themes/656787/assets/detail.js?1532588934329");
            }			
         </script>
         <script src='100/145/391/themes/656787/assets/plugin.js?1532588934329' type='text/javascript'></script>	
         <script src="bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
         <script src='100/145/391/themes/656787/assets/main.js?1532588934329' type='text/javascript'></script>				
      </div>
      <nav id="nav-mobile" class="hidden-md hidden-lg">
         <ul>
            <li>
               <a href="" title="">Trang chủ</a>
            </li>
            <li>
               <a href="collections/all" title="">Sản phẩm</a>
               <ul>
                  <li>
                     <a href="hoa-chuc-mung" title="">Hoa chúc mừng</a>
                     <ul>
                        <li><a href="gio-hoa-chuc-mung-1" title="">Giỏ hoa</a></li>
                        <li><a href="bo-hoa-chuc-mung" title="">Bó hoa</a></li>
                        <li><a href="hop-hoa-chuc-mung" title="">Hộp hoa</a></li>
                        <li><a href="ke-hoa-chuc-mung" title="">Kệ hoa</a></li>
                        <li><a href="binh-hoa-chuc-mung" title="">Bình hoa</a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="hoa-khai-truong" title="">Hoa khai trương</a>
                  </li>
                  <li>
                     <a href="hoa-sinh-nhat" title="">Hoa sinh nhật</a>
                  </li>
                  <li>
                     <a href="hoa-cam-on" title="">Hoa cảm ơn</a>
                  </li>
                  <li>
                     <a href="hoa-yeu-thuong" title="">Hoa yêu thương</a>
                  </li>
                  <li>
                     <a href="hoa-chia-buon" title="">Hoa chia buồn</a>
                  </li>
                  <li>
                     <a href="hoa-trang-tri-su-kien" title="">Hoa trang trí – sự kiện</a>
                  </li>
                  <li>
                     <a href="hoa-cuoi" title="">Hoa cưới</a>
                  </li>
               </ul>
            </li>
            <li>
               <a href="lan-ho-diep" title="">Lan Hồ Điệp</a>
            </li>
            <li>
               <a href="blog-tu-van" title="">Blog tư vấn</a>
            </li>
            <li>
               <a href="lien-he" title="">Liên hệ</a>
            </li>
         </ul>
      </nav>
   </body>
</html>
