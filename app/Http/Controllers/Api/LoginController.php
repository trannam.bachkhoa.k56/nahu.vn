<?php

namespace App\Http\Controllers\Api;
use App\Entity\Employee;
use App\Entity\EmployeeCareerCategories;
use App\Entity\Employer;
use App\Entity\EmployerRepresentative;
use App\Entity\EmployerTransaction;
use App\Ultility\Ultility;
use App\User;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
 use Facebook\Facebook;
use GuzzleHttp\Client;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->client = new Client();
    }
    public function index (Request $request) {
        $credentials = $request->only('email', 'password');
        try {
			$token = JWTAuth::attempt($credentials);
            if (!$token) {
                return response()->json([
                    'message' => 'Thông tin tài khoản hoặc mật khẩu không chính xác!'
                ], 401);
            }
            
        } catch (JWTException $exception) {
            return response()->json([
                'message' => 'Không thể tạo token. Vui lòng kiểm tra lại đường truyền!'
            ], 500);
        }
		
		$user = JWTAuth::toUser($token);
            $role = '';
            if (!empty($user)){
                $role = User::where('id', $user->id)->first()->role;
            }

            return response()->json(
                compact('token', 'role')
            );
    }

    public function getAuthUser(Request $request){
        try{
            $user = JWTAuth::toUser($request->token);
            return response()->json(['result' => $user]);
        }catch (JWTException $exception){
            return response()->json([
                'message' => 'Bạn cần đăng nhập để xem thông tin.'
            ], 400);
        }
    }

    public function checkAuthUser(Request $request) {
        try {
            $user = JWTAuth::toUser($request->token);
            return response()->json([
                'message' => 'Token vẫn có thể sử dụng.'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Token đã hết hạn sử dụng.'
            ], 400);
        }
    }

//    public function loginSocial(Request $request){
//        $providerId = $request->input('provider_id');
//        $email = $request->has('email') ? $request->input('email') : str_random(8) . '@social.com';
//
//        // Nếu đã đăng nhập từ trước thì sẽ trả ra luôn thông tin của người đó.
//        if(User::where('provider_id', $providerId)
//                ->orWhere('email', $email)->exists()){
//            $user = User::join('employees','employees.employee_user_id','=','users.id')
//                    ->where('users.provider_id', $providerId)
//                    ->orWhere('users.email', $email)
//                    ->select(
//                        'users.id',
//                        'users.email',
//                        'users.phone',
//                        'users.name',
//                        'users.role',
//                        'employees.employee_image'
//                    )
//                    ->first();
//            $token = JWTAuth::fromUser($user);
//            return response()->json([
//               'status' => '200',
//               'user' => $user,
//                'token' => $token,
//                'role' => $user->role
//            ]);
//        }
//
//        // Nếu chưa đăng nhập từ trước đó thì tạo tài khoản.
//        $user = $this->createUser($request);
//        $token = JWTAuth::fromUser($user);
//        return response()->json([
//            'status' => '200',
//            'user' => $user,
//            'token' => $token,
//            'role' => $user->role
//        ]);
//    }
//    public function loginSocial(Request $request, $provider)
//    {
//        if ($provider == 'google') {
//            return $this->checkGoogle($request->input('access_token'));
//        }
//
//        if ($provider == 'facebook') {
//            return $this->checkFacebook($request->input('access_token'));
//        }
//
//    }
//    private function checkFacebook($social_token){
//        try{
//            $checkToken = $this->client->get("https://graph.facebook.com/me?fields=id,name,email&access_token=$social_token");
//            $responseFacebook = json_decode($checkToken->getBody()->getContents(), true);
//            if(User::where('provider_id', $responseFacebook['id'])->exists()){
//                $user = User::where('provider_id', $responseFacebook['id'])->first();
//                $token = JWTAuth::fromUser($user);
//                return response()->json([
//                    "token"=>$token,
//                    "user"=> JWTAuth::toUser($token)
//                ]);
//            }
//            return response()->json([
//                "message"=>'Chưa đăng ký tài khoản facebook. Vui lòng đăng ký'
//            ]);
//        }catch (\Exception $e){
//            return response()->json([
//                "message"=>'Lỗi xảy ra. '.$e->getMessage()
//            ],404);
//        }
//    }
//    private function checkGoogle($google_id){
//        try{
//            if(User::where('provider_id', $google_id)->exists()){
//                $user = User::where('provider_id', $google_id)->first();
//                $token = JWTAuth::fromUser($user);
//                return response()->json([
//                    "token"=>$token,
//                    "user"=> JWTAuth::toUser($token)
//                ]);
//            }
//            return response()->json([
//                "message"=>'Chưa đăng ký tài khoản google. Vui lòng đăng ký'
//            ]);
//        }catch (\Exception $e){
//            return response()->json([
//                "message"=>'Lỗi xảy ra. '.$e->getMessage()
//            ],404);
//        }
//    }
    public function loginSocial(Request $request){
        try{
            $tokenFB = $request->input('tokenFB');
            $checkToken = $this->client->get("https://graph.facebook.com/me?fields=id,name,email&access_token=$tokenFB");
            $responseFacebook = json_decode($checkToken->getBody()->getContents(), true);
            $idFace =  $responseFacebook['id'];
            if(User::where('provider_id', $responseFacebook['id'])->exists()){
                $user = User::where('provider_id', $responseFacebook['id'])->first();
                $token = JWTAuth::fromUser($user);
                return response()->json([
                    "token"=>$token,
                    "user"=> JWTAuth::toUser($token)
                ]);
            }
            return response()->json([
                "message"=>'Chưa đăng ký facebook. Vui lòng đăng ký'
            ]);
        }catch (\Exception $e){
            return response()->json([
                "message"=>'Lỗi xảy ra. '.$e->getMessage()
            ],404);
        }

    }
    public function registerSocial(Request $request){
        if (!$request->has('email')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập email.',
                'data' => []
            ], 400);
        }
        if (!$request->has('password')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập mật khẩu',
                'data' => []
            ], 400);
        }
        if (!$request->has('name')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập họ và tên',
                'data' => []
            ], 400);
        }
        if (!$request->has('phone')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập số điện thoại',
                'data' => []
            ], 400);
        }
        if (!$request->has('address')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập vào địa chỉ của bạn',
                'data' => []
            ], 400);
        }
        if (!$request->has('careers')){
            return response()->json([
                'message' => 'Bạn vui lòng chọn ngành nghề của bạn',
                'data' => []
            ], 400);
        }
        if (!$request->has('experience')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập vào kinh nghiệm của bạn',
                'data' => []
            ], 400);
        }
        if(User::where('email', $request->input('email'))->exists()){
            return response()->json([
                'message' => 'Email đã đăng ký. Mời bạn nhập một email khác.',
                'data' => []
            ], 400);
        }

        if(User::where('phone', $request->input('phone'))->exists()){
            return response()->json([
                'message' => 'Số điện thoại đã đăng ký. Mời bạn nhập một số điện thoại khác.',
                'data' => []
            ], 400);
        }
        try{
            $tokenFB = $request->input('tokenFB');
            $checkToken = $this->client->get("https://graph.facebook.com/me?fields=id,name,email&access_token=$tokenFB");
            $responseFacebook = json_decode($checkToken->getBody()->getContents(), true);
            if(!User::where('provider_id', $responseFacebook['id'])->exists()){
                //add to employer
                $userID = $this->insertUser($request,$responseFacebook['id']);
                $employerID = $this->createEmployer($request, $userID);
                $this->insertRepresentative($request, $employerID);
                //add to employee
                $employeeID = $this->insertEmployee($request, $userID);
                $this->insertEmployeeCareer($request, $employeeID);
                // new user
//            $token = JWTAuth::fromUser(User::getUserByUserId($userID));
                $user = new User();
                $user = $user->where('id', $userID)->first();
                $token = JWTAuth::fromUser($user);
                return response()->json([
                    'message' => 'Chúc mừng bạn đã tạo tài khoản qua Facebook thành công.',
                    'token' => $token,
                    'user' => $user
                ], 200);
            }
            return response()->json([
                "message"=>'Tài khoản này đã đăng ký Facebook. Vui lòng đăng nhập'
            ]);
        }catch (\Exception $e){
            return response()->json([
                "message"=>'Lỗi xảy ra. '.$e->getMessage()
            ],404);
        }

    }
    public function loginWithGoogle(Request $request){
        try{
            if(User::where('provider_id', $request->input('google_id'))->exists()){
                $user = User::where('provider_id',  $request->input('google_id'))->first();
                $token = JWTAuth::fromUser($user);
                return response()->json([
                    "token"=>$token,
                    "user"=> JWTAuth::toUser($token)
                ]);
            }
            return response()->json([
                "message"=>'Chưa đăng ký tài khoản google. Vui lòng đăng ký'
            ]);
        }catch (\Exception $e){
            return response()->json([
                "message"=>'Lỗi xảy ra. '.$e->getMessage()
            ],404);
        }
    }
    public function registerWithGoogle(Request $request){
        if (!$request->has('email')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập email.',
                'data' => []
            ], 400);
        }
        if (!$request->has('password')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập mật khẩu',
                'data' => []
            ], 400);
        }
        if (!$request->has('name')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập họ và tên',
                'data' => []
            ], 400);
        }
        if (!$request->has('phone')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập số điện thoại',
                'data' => []
            ], 400);
        }
        if (!$request->has('address')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập vào địa chỉ của bạn',
                'data' => []
            ], 400);
        }
        if (!$request->has('careers')){
            return response()->json([
                'message' => 'Bạn vui lòng chọn ngành nghề của bạn',
                'data' => []
            ], 400);
        }
        if (!$request->has('experience')){
            return response()->json([
                'message' => 'Bạn vui lòng nhập vào kinh nghiệm của bạn',
                'data' => []
            ], 400);
        }
        if(User::where('email', $request->input('email'))->exists()){
            return response()->json([
                'message' => 'Email đã đăng ký. Mời bạn nhập một email khác.',
                'data' => []
            ], 400);
        }

        if(User::where('phone', $request->input('phone'))->exists()){
            return response()->json([
                'message' => 'Số điện thoại đã đăng ký. Mời bạn nhập một số điện thoại khác.',
                'data' => []
            ], 400);
        }
        try{
            $googleID = $request->input('google_id');
            if(!User::where('provider_id', $googleID)->exists()){
                //add to employer
                $userID = $this->insertUser($request,$googleID);
                $employerID = $this->createEmployer($request, $userID);
                $this->insertRepresentative($request, $employerID);
                //add to employee
                $employeeID = $this->insertEmployee($request, $userID);
                $this->insertEmployeeCareer($request, $employeeID);
                // new user
//            $token = JWTAuth::fromUser(User::getUserByUserId($userID));
                $user = new User();
                $user = $user->where('id', $userID)->first();
                $token = JWTAuth::fromUser($user);
                return response()->json([
                    'message' => 'Chúc mừng bạn đã tạo tài khoản qua Google thành công.',
                    'token' => $token,
                    'user' => $user
                ], 200);
            }
            return response()->json([
                "message"=>'Tài khoản này đã đăng ký Google. Vui lòng đăng nhập'
            ]);
        }catch (\Exception $e){
            return response()->json([
                "message"=>'Lỗi xảy ra. '.$e->getMessage()
            ],404);
        }

    }
//    public function registerSocial(Request $request, $provider){
//        if ($provider == 'google') {
//            return $this->registerGoogle($request, $request->input('access_token'));
//        }
//
//        if ($provider == 'facebook') {
//            return $this->registerFacebook($request, $request->input('access_token'));
//        }
//    }
//    private function registerGoogle($request,$google_id){
//        if (!$request->has('email')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập email.',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('password')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập mật khẩu',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('name')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập họ và tên',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('phone')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập số điện thoại',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('address')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập vào địa chỉ của bạn',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('careers')){
//            return response()->json([
//                'message' => 'Bạn vui lòng chọn ngành nghề của bạn',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('experience')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập vào kinh nghiệm của bạn',
//                'data' => []
//            ], 400);
//        }
//        if(User::where('email', $request->input('email'))->exists()){
//            return response()->json([
//                'message' => 'Email đã đăng ký. Mời bạn nhập một email khác.',
//                'data' => []
//            ], 400);
//        }
//
//        if(User::where('phone', $request->input('phone'))->exists()){
//            return response()->json([
//                'message' => 'Số điện thoại đã đăng ký. Mời bạn nhập một số điện thoại khác.',
//                'data' => []
//            ], 400);
//        }
//        try{
//
//            if(!User::where('provider_id', $google_id)->exists()){
//                //add to employer
//                $userID = $this->insertUser($request,$google_id);
//                $employerID = $this->createEmployer($request, $userID);
//                $this->insertRepresentative($request, $employerID);
//                //add to employee
//                $employeeID = $this->insertEmployee($request, $userID);
//                $this->insertEmployeeCareer($request, $employeeID);
//                // new user
////            $token = JWTAuth::fromUser(User::getUserByUserId($userID));
//                $user = new User();
//                $user = $user->where('id', $userID)->first();
//                $token = JWTAuth::fromUser($user);
//                return response()->json([
//                    'message' => 'Chúc mừng bạn đã tạo tài khoản qua Google thành công.',
//                    'token' => $token,
//                    'user' => $user
//                ], 200);
//            }
//            return response()->json([
//                "message"=>'Tài khoản này đã đăng ký Google. Vui lòng đăng nhập'
//            ]);
//        }catch (\Exception $e){
//            return response()->json([
//                "message"=>'Lỗi xảy ra. '.$e->getMessage()
//            ],404);
//        }
//    }
//    private function registerFacebook($request,$access_token){
//        if (!$request->has('email')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập email.',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('password')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập mật khẩu',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('name')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập họ và tên',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('phone')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập số điện thoại',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('address')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập vào địa chỉ của bạn',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('careers')){
//            return response()->json([
//                'message' => 'Bạn vui lòng chọn ngành nghề của bạn',
//                'data' => []
//            ], 400);
//        }
//        if (!$request->has('experience')){
//            return response()->json([
//                'message' => 'Bạn vui lòng nhập vào kinh nghiệm của bạn',
//                'data' => []
//            ], 400);
//        }
//        if(User::where('email', $request->input('email'))->exists()){
//            return response()->json([
//                'message' => 'Email đã đăng ký. Mời bạn nhập một email khác.',
//                'data' => []
//            ], 400);
//        }
//
//        if(User::where('phone', $request->input('phone'))->exists()){
//            return response()->json([
//                'message' => 'Số điện thoại đã đăng ký. Mời bạn nhập một số điện thoại khác.',
//                'data' => []
//            ], 400);
//        }
//        try{
//            $checkToken = $this->client->get("https://graph.facebook.com/me?fields=id,name,email&access_token=$access_token");
//            $responseFacebook = json_decode($checkToken->getBody()->getContents(), true);
//            if(!User::where('provider_id', $responseFacebook['id'])->exists()){
//                //add to employer
//                $userID = $this->insertUser($request,$responseFacebook['id']);
//                $employerID = $this->createEmployer($request, $userID);
//                $this->insertRepresentative($request, $employerID);
//                //add to employee
//                $employeeID = $this->insertEmployee($request, $userID);
//                $this->insertEmployeeCareer($request, $employeeID);
//                // new user
////            $token = JWTAuth::fromUser(User::getUserByUserId($userID));
//                $user = new User();
//                $user = $user->where('id', $userID)->first();
//                $token = JWTAuth::fromUser($user);
//                return response()->json([
//                    'message' => 'Chúc mừng bạn đã tạo tài khoản qua Facebook thành công.',
//                    'token' => $token,
//                    'user' => $user
//                ], 200);
//            }
//            return response()->json([
//                "message"=>'Tài khoản này đã đăng ký Facebook. Vui lòng đăng nhập'
//            ]);
//        }catch (\Exception $e){
//            return response()->json([
//                "message"=>'Lỗi xảy ra. '.$e->getMessage()
//            ],404);
//        }
//    }
    private function createUser($request){
        $email = $request->has('email') ? $request->input('email') : str_random(8) . '@social.com';
        $name = $request->has('name') ? $request->input('name') : 'Người dùng FaceBook';
        $userID = User::insertGetId([
            'name' => $name,
            'role' => 1,
            'email' => $email,
            'password' => bcrypt(str_random(10)),
            'provider_id' => $request->input('provider_id'),
            'created_at' => new \DateTime()
        ]);

        Employee::insert([
            'employee_name' => $name,
            'email' => $email,
            'employee_user_id' => $userID,
            'created_at' => new \DateTime()
        ]);

        $newUser = User::where('id', $userID)->first();

        return $newUser;
    }
    private function insertUser(Request $request,$providerID){
        $user = new User();
        return  $user->insertGetId([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'phone'=> $request->input('phone'),
            'address' => $request->input('address'),
            'role' => 2,
            'provider_id' => $providerID,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
    private function createEmployer(Request $request, $userID){
        $employer = new Employer();
        $employerID = $employer->insertGetId([
            'enterprise_name' => $request->input('name'),
            'employer_user_id'=> $userID,
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'total_money' => 200000,
            'address' => $request->input('address'),
            'created_at' => new \DateTime()
        ]);

        $slug = Ultility::createSlug($request->input('name'));
        if(!empty($employer->where('slug', $slug)->first())){
            $slug .= '-' . $employerID;
        }

        $employer->where('employer_id', $employerID)->update([
            'slug' => $slug
        ]);
        EmployerTransaction::insert([
            'employer_id' => $employerID,
            'money' =>  200000,
            'reason' => 'Tặng 200k tạo tài khoản lần đầu',
            'created_at' => new \DateTime()
        ]);
        return $employerID;
    }
    private function insertRepresentative(Request $request, $employerID){
        $representative = new EmployerRepresentative();
        $representative->insert([
            'employer_id' =>$employerID,
            'representative_name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
    private function insertEmployee(Request $request, $userID){
        $employee = new Employee();
        return $employee->insertGetId([
            'employee_name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'experience' => $request->input('experience'),
            'employee_user_id' => $userID,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
    private function insertEmployeeCareer(Request $request, $employeeId){
        $careerArray = $request->input('careers');
        foreach($careerArray as $career){
            EmployeeCareerCategories::insert([
                'employee_id' => $employeeId,
                'career_category_id' => $career,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        }
    }
    public function loginWithPhone(Request $request) {
        try {
            $token = JWTAuth::attempt([
                'phone' => $request->phone,
                'password' => $request->password
            ]);
            if (!$token) {
                return response()->json([
                    'message' => 'Thông tin tài khoản hoặc mật khẩu không chính xác'
                ], 401);
            }

        } catch (JWTException $exception) {
            return response()->json([
                'message' => 'Không thể tạo token. Đã có lỗi xảy ra'
            ], 500);
        }

        $user = JWTAuth::toUser($token);
        $role = '';
        if (!empty($user)){
            $role = User::where('id', $user->id)->first()->role;
        }

        return response()->json(
            compact('token', 'role')
        );
    }
}
