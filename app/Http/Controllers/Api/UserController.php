<?php

namespace App\Http\Controllers\Api;

use App\Entity\MailConfig;
use App\Ultility\Ultility;
use App\Entity\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class UserController extends Controller
{
    public function register(Request $request) {

        $validation = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|string|unique:users',
            'phone' => 'required|unique:users|max:10',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validation->fails()) {
            return response() ->json([
                'message' => 'Lỗi dữ liệu truyền lên.',
                'errors' => $validation->errors()
            ], 404);
        }

        // try {
            //add to employer
            $userID = $this->insertUser($request);
            // new user
            $user = new User();
            $user = $user->where('id', $userID)->first();
            $token = JWTAuth::fromUser($user);

            return response()->json([
                'message' => 'Chúc mừng bạn đã tạo tài khoản thành công.',
                'data' => $token,
                'user' => $user
            ], 200);

        // } catch (\Exception $exception) {
        //     return response() ->json([
        //         'message' => 'Lỗi đường truyền.',
        //         'data' => []
        //     ], 404);
        // }
    }

    private function insertUser(Request $request){
        $user = new User();
        return  $user->insertGetId([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'phone'=> $request->input('phone'),
            'address' => $request->input('address'),
            'role' => 2,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }

    public function forgetPass(Request $request){
        // try{
            $user = User::where('email', $request->input('email'))->first();

            if(!empty($user)) {
                $this->ResetPass($user->id, $user->phone);
                $message = 'Tài khoản '.$user->email.' đã đặt lại mật khẩu vào lúc '.Carbon::now()->toDateTimeString().' .Mật khẩu mới của bạn chính là số điện thoại của bạn! Bạn vui lòng truy cập và đổi lại mật khẩu nhé!' ;

//                MailConfig::sendMail($user->email, 'Thư thông báo đặt lại mật khẩu', $message);
                return response()->json([
                    'message' => $message
                ], 200);
            }

            return response()->json([
                'message' => 'Email của bạn không đúng hoặc chưa được đăng ký.'
            ], 404);
        // }catch (\Exception $exception){
        //     return response()->json([
        //         'message' => 'Đã có lỗi xảy ra : ' . $exception->getMessage()
        //     ], 404);
        // }
    }

    private function ResetPass($id, $phone) {
        User::where('id',$id)->update(
            [
                'password'=>bcrypt($phone)
            ]
        );
    }


}
