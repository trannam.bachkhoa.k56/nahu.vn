<?php
namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Entity\User;
use Carbon\Carbon;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }


        return $this->respondWithToken($token);
    }

    public function register(Request $request) {
        $validation = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|string|unique:users',
            'phone' => 'required|unique:users|max:10',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validation->fails()) {
            return response() ->json([
                'message' => 'Lỗi dữ liệu truyền lên.',
                'errors' => $validation->errors()
            ], 404);
        }

        // try {
        //add to employer
        $userID = $this->insertUser($request);
        $credentials = request(['email', 'password']);
        $token = auth('api')->attempt($credentials);

        return $this->respondWithToken($token);
    }

    private function insertUser(Request $request){
        $user = new User();
        return  $user->insertGetId([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'phone'=> $request->input('phone'),
            'address' => $request->input('address'),
            'role' => 2,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 6000
        ]);
    }

    public function forgetPass(Request $request){
        // try{
        $user = User::where('email', $request->input('email'))->first();

        if(!empty($user)) {
            $this->ResetPass($user->id, $user->phone);
            $message = 'Tài khoản '.$user->email.' đã đặt lại mật khẩu vào lúc '.Carbon::now()->toDateTimeString().' .Mật khẩu mới của bạn chính là số điện thoại của bạn! Bạn vui lòng truy cập và đổi lại mật khẩu nhé!' ;

//                MailConfig::sendMail($user->email, 'Thư thông báo đặt lại mật khẩu', $message);
            return response()->json([
                'message' => $message
            ], 200);
        }

        return response()->json([
            'message' => 'Email của bạn không đúng hoặc chưa được đăng ký.'
        ], 404);
        // }catch (\Exception $exception){
        //     return response()->json([
        //         'message' => 'Đã có lỗi xảy ra : ' . $exception->getMessage()
        //     ], 404);
        // }
    }

    private function ResetPass($id, $phone) {
        User::where('id',$id)->update(
            [
                'password'=>bcrypt($phone)
            ]
        );
    }
}