<?php


namespace App\Http\Controllers\Site;

use App\Entity\Dictionaries;
use Illuminate\Http\Request;

class LearnEnglishController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index(Request $request) {
        // 1 từ gồm: 2 cách đọc + 1 ví dụ + 1 nghĩa
        $dictionaries = Dictionaries::whereIn('dictionary_id', $request->input('dictionary_id'))->get();
        // sinh ra: 30 câu / số từ = số câu hỏi liên quan đến từ.
        // số từ.
        $numberWords = count($request->input('dictionary_id'));
        // Số câu hỏi:
        $numberQuestions = ceil(30 / $numberWords);

        // Random câu hỏi / từ =  cách từ 0 đến 25. nếu = 6 thì lấy ra vị trí 1 và 0: từ là đọc uk và từ mới.
        $questions = array();
        foreach ($dictionaries as $dictionary) {
            $questions = $this->randomQuestion($numberQuestions, $dictionary, $dictionaries, $questions);
        }

        // trả về cách học 30 câu hỏi trong số câu hỏi đã tạo ra tại questions.
        return view ('site.learn_english.index', compact('questions'));
    }

    private function randomQuestion ($numberQuestions, $word, $dictionaries, $questions) {
        for ($i = 0; $i <= $numberQuestions; $i++) {
            $question = array();
            // chọn ra câu hỏi
            $rand = rand(0, 25);

            // câu hỏi
            switch (floor($rand/5)) {
                case 0:
                    $question['question'] = $word->word;
                    break;
                case 1:
                    $question['question'] = $word->uk_audio;
                    break;
                case 2:
                    $question['question'] = $word->us_audio;
                    break;
                case 3:
                    $question['question'] = $word->example;
                    break;
                default:
                    $question['question'] = $word->mean;
                    break;
            }

            // câu trả lời
            switch ($rand % 5) {
                case 0:
                    $question['answer'][] = $word->word;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] = $dictionary->word;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
                case 1:
                    $question['answer'][] = $word->uk_audio;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] = $dictionary->uk_audio;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
                case 2:
                    $question['answer'][] = $word->us_audio;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] = $dictionary->us_audio;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
                case 3:
                    $question['answer'][] = $word->example;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] = $dictionary->example;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
                default:
                    $question['answer'][] = $word->mean;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] = $dictionary->mean;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
            }

            shuffle($question['answer']);
            $questions[] = $question;

        }

        return $questions;
    }
}