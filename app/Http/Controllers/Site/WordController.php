<?php


namespace App\Http\Controllers\Site;

use App\Entity\Dictionaries;
use Illuminate\Http\Request;
use KubAT\PhpSimple\HtmlDomParser;

class WordController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function search (Request $request) {
        $word = strtolower($request->input('word'));

        if ($request->has('search')) {
            $dictionary = Dictionaries::where('word', $word)
                ->first();

            if (empty($dictionary)) {
                $dictionary = $this->getWordFromInternet($word);

                if ($dictionary == false) {
                    return response([
                        'status' => 500
                    ])->header('Content-Type', 'text/plain');
                }
            }
			
            // check xem session từ khóa đã lưu chưa
            $insertDone = $this->insertDictionaries($dictionary->dictionary_id, $request);

            if ($insertDone) {
                return response([
                    'status' => 200,
                    'dictionary' => $dictionary,
                ])->header('Content-Type', 'text/plain');
            }

            return response([
                'status' => 300,
            ])->header('Content-Type', 'text/plain');
        }

        $dictionaries = Dictionaries::where('word', 'like', $word.'%')
            ->select('word', 'dictionary_id')
            ->limit(5)
            ->get()->toArray();

        // tra ve ket qua
        return response([
            'status' => 200,
            'dictionaries' => $dictionaries,
        ])->header('Content-Type', 'text/plain');
    }

    private function getWordFromInternet($word) {
        try {
            $html = HtmlDomParser::file_get_html('https://dictionary.cambridge.org/dictionary/english/'.$word);
            $dataWord = [];
            // lấy ra phát âm tiếng uk
            foreach($html->find('.uk amp-audio source') as $element) {
                if ($element->attr['type'] == 'audio/mpeg') {
                    $dataWord['uk_audio'] = 'https://dictionary.cambridge.org/'.$element->attr['src'];
                    //echo 'https://dictionary.cambridge.org/'.$element->attr['src'];
                }
            }

            // lấy ra phát âm tiếng us
            foreach($html->find('.us amp-audio source') as $element) {
                if ($element->attr['type'] == 'audio/mpeg') {
                    $dataWord['us_audio'] = 'https://dictionary.cambridge.org/'.$element->attr['src'];
                    //echo 'https://dictionary.cambridge.org/'.$element->attr['src'];
                }
            }

            // Cách phát âm uk
            foreach($html->find('.uk .pron') as $element) {
                $dataWord['uk_pron'] = $element->innertext;
                //echo $element->innertext;
            }

            // Cách phát âm us
            foreach($html->find('.us .pron') as $element) {
                $dataWord['us_pron'] = $element->innertext;
            }

            // lấy ra ví dụ
            $example = '';
            foreach($html->find('.examp  .eg ') as $id => $element) {
                if ($id < 5) {
                    $example .= strip_tags($element->innertext). '<br>';
                    //echo strip_tags($element->innertext);
                    continue;
                }

                break;
            }
            $dataWord['example'] = $example;

            // lấy ra nghĩa của từ:
            $html = HtmlDomParser::file_get_html('http://tratu.coviet.vn/hoc-tieng-anh/tu-dien/lac-viet/A-V/'.$word.'.html');
            foreach($html->find('#partofspeech_0') as $element) {
                $dataWord['mean'] = strip_tags($element->innertext, '<div><span>');
                // echo strip_tags($element->innertext, '<div><span>');
            }

            // lưu toàn bộ nội dung từ đó vào database
            $dataWord['created_at'] = new \DateTime();
            $dataWord['word'] = $word;

            $dictionaryId = Dictionaries::insertGetId($dataWord);

            $dictionary = Dictionaries::where('dictionary_id', $dictionaryId)->first();

            return $dictionary;
        } catch (\Exception $e) {
            return false;
        }
    }
    public function getDetailWord (Request $request) {
        $dictionaryId = $request->input('dictionary_id');
        $dictionary = Dictionaries::where('dictionary_id', $dictionaryId)
            ->first();

        // check xem session từ khóa đã lưu chưa
        $insertDone = $this->insertDictionaries($dictionaryId, $request);

        if ($insertDone) {
            return response([
                'status' => 200,
                'dictionary' => $dictionary,
            ])->header('Content-Type', 'text/plain');
        }

        return response([
            'status' => 500,
        ])->header('Content-Type', 'text/plain');
    }

    private function insertDictionaries ($dictionaryId, $request) {
        // check xem session từ khóa đã lưu chưa
        $dictionariesSessions = $request->session()->pull('dictionaries');
        $existDictionary = 0;
        if (!empty($dictionariesSessions)) {
            foreach ($dictionariesSessions as $dictionariesSession) {
                if ($dictionaryId == $dictionariesSession) {
                    $existDictionary  = 1;
                }

                $request->session()->push('dictionaries', $dictionariesSession);
            }
        }
        // nếu chưa lưu thì lưu vào session
        if ($existDictionary == 0) {
            $request->session()->push('dictionaries', $dictionaryId);

            // tra ve ket qua
            return true;
        }

        return false;
    }
    public function deleteWord (Request $request){
        $dictionaryId = $request->input('dictionary_id');

        // xóa trong session lưu từ
        $dictionariesSessions = $request->session()->pull('dictionaries');
        if (!empty($dictionariesSessions)) {
            foreach ($dictionariesSessions as $dictionariesSession) {
                if ($dictionaryId == $dictionariesSession) {
                    continue;
                }

                $request->session()->push('dictionaries', $dictionariesSession);
            }
        }

        // tra ve ket qua
        return response([
            'status' => 200,
        ])->header('Content-Type', 'text/plain');
    }

    public function cleanWord (Request $request) {
        $request->session()->pull('dictionaries');

        return redirect('/');
    }

}