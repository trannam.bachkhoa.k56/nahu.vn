<?php


namespace App\Http\Controllers\Site;


use App\Entity\Dictionaries;
use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionController extends SiteController
{
    public function index (Request $request) {
        $request->session()->pull('questionsSave');
        // 1 từ gồm: 2 cách đọc + 1 ví dụ + 1 nghĩa
        $dictionaries = Dictionaries::whereIn('dictionary_id', $request->input('dictionary_id'))->distinct()->get();
        // sinh ra: 30 câu / số từ = số câu hỏi liên quan đến từ.
        // số từ.
        $numberWords = count($request->input('dictionary_id'));
		
        // nếu số từ học >= 10 check xem họ đã đăng ký chưa.
        //$checkLogin = $this->checkLogin($numberWords);

        // chuyển trạng thái đăng nhập nếu họ học hơn 10 từ.
        /*if ($checkLogin) {
            return redirect('/trang/dang-nhap')->withErrors(['Bạn đã đăng ký học quá 10 từ 1 lúc, vui lòng đăng nhập để có thể tiếp tục sử dụng tính năng này!']);
        }*/

        // Số câu hỏi:
        $numberQuestions = ceil(30 / $numberWords);

        // Random câu hỏi / từ =  cách từ 0 đến 25. nếu = 6 thì lấy ra vị trí 1 và 0: từ là đọc uk và từ mới.
        $questions = array();
        foreach ($dictionaries as $dictionary) {
            $questions = $this->randomQuestion($numberQuestions, $dictionary, $dictionaries, $questions, $request);
        }

        shuffle($questions);

        // lưu vào session tí check kết quả
        foreach ($questions as $question) {
            $request->session()->push('questionsSave', $question);
        }

        /*$checkTimeLearn = $this->checkTimeLearn();
        if ($checkTimeLearn) {
            return redirect ('/trang/thanh-toan');
        } */

        return view('site.test.question_answer', compact('questions'));
    }

    private function checkTimeLearn () {
        if (!Auth::check()) {
            return false;
        }

        if ( (Auth::user()->time > 5)
            && (strtotime(Auth::user()->end_at) < time()) ) {
            return true;
        }

        User::where('id', Auth::id())->update([
            'time' => (Auth::user()->time + 1)
        ]);

        return false;
    }

    private function checkLogin($numberWords) {
        if ($numberWords < 10) {
            return false;
        }

        if (Auth::check()) {
            return false;
        }

        return true;
    }

    public function result(Request $request) {
        $questions = $request->session()->pull('questionsSave');
        if (empty($questions)) {
            return redirect('/');
        }
        $answers = $request->input('answer');
        $point = 0;
        $numberQuestions = count($questions);
        foreach ($answers as $id => $answer) {
            if ($answer == $questions[$id]['answer_right']) {
                $point ++;
            }
        }

        return view('site.test.result', compact('point', 'numberQuestions'));
    }

    private function randomQuestion ($numberQuestions, $word, $dictionaries, $questions, $request) {
        for ($i = 0; $i < $numberQuestions; $i++) {
            $question = array();
            // chọn ra câu hỏi
            $rand = rand(0, 25);

            // câu hỏi
            switch (floor($rand/5)) {
                case 0:
                    $question['question'] = $word->word;
                    break;
                case 1:
                    $question['question'] = !empty($word->uk_audio) ? '<audio controls>
                                                <source src="'.$word->uk_audio.'" type="audio/mpeg">
                                            </audio>' : $word->word;
                    break;
                case 2:
                    $question['question'] = !empty($word->us_audio) ? '<audio controls>
                                                <source src="'.$word->us_audio.'" type="audio/mpeg">
                                            </audio>' : $word->word;
                    break;
                case 3:
                    $question['question'] = !empty($word->example) ? $word->example :  $word->word;
                    break;
                default:
                    $question['question'] = !empty($word->mean) ? $word->mean : $word->word;
                    break;
            }

            if (floor($rand/5) == $rand % 5) {
                $rand += 1;
            }
            // câu trả lời
            switch ($rand % 5) {
                case 0:
                    $question['answer'][] = $word->word;
                    $question['answer_right'] = $word->word;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] = $dictionary->word;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
                case 1:
                    $question['answer'][] = !empty($word->uk_audio) ? '<audio controls>
                                                <source src="'.$word->uk_audio.'" type="audio/mpeg">
                                            </audio>' : $word->word;

                    $question['answer_right'] = !empty($word->uk_audio) ? '<audio controls>
                                                <source src="'.$word->uk_audio.'" type="audio/mpeg">
                                            </audio>' : $word->word;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] =  !empty($dictionary->uk_audio) ? '<audio controls>
                                                <source src="'.$dictionary->uk_audio.'" type="audio/mpeg">
                                            </audio>' : $dictionary->word;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
                case 2:
                    $question['answer'][] = !empty($word->us_audio) ?  '<audio controls>
                                                <source src="'.$word->us_audio.'" type="audio/mpeg">
                                            </audio>' : $word->word;
                    $question['answer_right'] = !empty($word->us_audio) ?  '<audio controls>
                                                <source src="'.$word->us_audio.'" type="audio/mpeg">
                                            </audio>' : $word->word;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] = !empty($dictionary->us_audio) ?  '<audio controls>
                                                <source src="'.$dictionary->us_audio.'" type="audio/mpeg">
                                            </audio>' : $dictionary->word;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
                case 3:
                    $question['answer'][] = !empty($word->example) ? $word->example :  $word->word;;
                    $question['answer_right'] = !empty($word->example) ? $word->example :  $word->word;;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] = !empty($dictionary->example) ? $dictionary->example :  $word->word;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
                default:
                    $question['answer'][] = !empty($word->mean) ? $word->mean :  $word->word;
                    $question['answer_right'] =  !empty($word->mean) ? $word->mean :  $word->word;
                    $idx = 0;
                    foreach ($dictionaries as $dictionary) {
                        if ($dictionary->word  != $word->word) {
                            $question['answer'][] =  !empty($dictionary->mean) ? $dictionary->mean :  $word->word;
                            $idx ++;
                        }
                        if ($idx == 3) {
                            break;
                        }
                    }
                    break;
            }

            shuffle($question['answer']);
            foreach ($question['answer'] as $id => $answer) {
                if ($question['answer_right'] == $answer) {
                    $question['answer_right'] = $id;
                    break;
                }
            }
            $questions[] = $question;
        }

        return $questions;
    }
}