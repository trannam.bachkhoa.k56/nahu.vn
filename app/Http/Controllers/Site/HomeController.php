<?php

namespace App\Http\Controllers\Site;

use App\Entity\Dictionaries;
use Illuminate\Http\Request;
use Yangqi\Htmldom\Htmldom;

class HomeController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dictionariesSession =  session('dictionaries');
        $dictionaries = array();
        if (!empty($dictionariesSession)) {
            $dictionaries = Dictionaries::whereIn('dictionary_id', $dictionariesSession)
                ->get();
        }

        return view('site.default.index', compact('dictionaries'));
    }
}
