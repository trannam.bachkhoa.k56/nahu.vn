<?php


namespace App\Http\Controllers\Site;

use App\Entity\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends SiteController
{
    public function index (Request $request) {
        $payment = $request->input('payment');
        $message = $request->input('message');

        if (!Auth::check()) {
            return redirect()->back()->withErrors(['Bạn vui lòng đăng nhập để có thể tiến hành thanh toán!']);
        }

        $order = new Order();
        $orderId = $order->insertGetId([
            'status' => '1', // trang thai đặt hàng thành công
            'shipping_name' => Auth::user()->name,
            'shipping_email' => Auth::user()->email,
            'shipping_phone' => Auth::user()->phone,
            'shipping_note' => $message,
            'total_price' => $payment,
            'created_at' =>   new \DateTime(),
            'updated_at' =>   new \DateTime(),
            'user_id' => Auth::id()
        ]);


        return view ('site.payment.index', compact('orderId', 'payment'));
    }
}