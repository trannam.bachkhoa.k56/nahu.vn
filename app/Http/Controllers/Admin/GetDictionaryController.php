<?php


namespace App\Http\Controllers\Admin;

use App\Entity\Dictionaries;
use App\Entity\Edict;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Entity\User;
use KubAT\PhpSimple\HtmlDomParser;

class GetDictionaryController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });

    }

    public function index(Request $request) {

        return view('admin.dictionaries.index');
    }

    public function htmlDom(Request $request) {
        $start = $request->input('start');
        $end = $request->input('end');

        if ($start > $end) {
            return response([
                'status' => 500,
            ])->header('Content-Type', 'text/plain');
        }

        // lấy từ mới
        $this->updateDictionary($start);

//        }catch (\Exception $exception){
//            return $exception->getMessage();
//        }

        return response([
            'status' => 200,
        ])->header('Content-Type', 'text/plain');
    }

    private function updateDictionary ($start) {
        $edicts = Edict::offset($start)->limit(5)->get();

        foreach ($edicts as $edict) {
            // lấy ra toàn bộ nội dung của 1 từ.
            $dataWord = $this->htmlDomWord($edict->word);

            // nếu không lấy được từ thì bỏ qua
            if (empty($dataWord)) {
                continue ;
            }

            // lưu toàn bộ nội dung từ đó vào database
            $dataWord['created_at'] = new \DateTime();
            $dataWord['word'] = $edict->word;

            Dictionaries::insert($dataWord);
        }

        return ;
    }

    private function htmlDomWord($word) {
        try {
            $html = HtmlDomParser::file_get_html('https://dictionary.cambridge.org/dictionary/english/'.$word);
            $dataWord = [];
            // lấy ra phát âm tiếng uk
            foreach($html->find('.uk amp-audio source') as $element) {
                if ($element->attr['type'] == 'audio/mpeg') {
                    $dataWord['uk_audio'] = 'https://dictionary.cambridge.org/'.$element->attr['src'];
                    //echo 'https://dictionary.cambridge.org/'.$element->attr['src'];
                }
            }

            // lấy ra phát âm tiếng us
            foreach($html->find('.us amp-audio source') as $element) {
                if ($element->attr['type'] == 'audio/mpeg') {
                    $dataWord['us_audio'] = 'https://dictionary.cambridge.org/'.$element->attr['src'];
                    //echo 'https://dictionary.cambridge.org/'.$element->attr['src'];
                }
            }

            // Cách phát âm uk
            foreach($html->find('.uk .pron') as $element) {
                $dataWord['uk_pron'] = $element->innertext;
                //echo $element->innertext;
            }

            // Cách phát âm us
            foreach($html->find('.us .pron') as $element) {
                $dataWord['us_pron'] = $element->innertext;
            }

            // lấy ra ví dụ
            $example = '';
            foreach($html->find('.examp  .eg ') as $id => $element) {
                if ($id < 5) {
                    $example .= strip_tags($element->innertext). '<br>';
                    //echo strip_tags($element->innertext);
                    continue;
                }

                break;
            }
            $dataWord['example'] = $example;

            // lấy ra nghĩa của từ:
            $html = HtmlDomParser::file_get_html('http://tratu.coviet.vn/hoc-tieng-anh/tu-dien/lac-viet/A-V/'.$word.'.html');
            foreach($html->find('#partofspeech_0') as $element) {
                $dataWord['mean'] = strip_tags($element->innertext, '<div><span>');
                // echo strip_tags($element->innertext, '<div><span>');
            }

            return $dataWord;
        } catch (\Exception $e) {
            return array();
        }
    }

}