<?php


namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Dictionaries extends Model
{
    protected $table = 'dictionaries';

    protected $primaryKey = 'dictionary_id';

    protected $fillable = [
        'dictionary_id',
        'word',
        'uk_audio',
        'us_audio',
        'uk_pron',
        'us_pron',
        'example',
        'mean',
        'created_at'
    ];
}