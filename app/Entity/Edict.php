<?php


namespace App\Entity;


use Illuminate\Database\Eloquent\Model;

class Edict extends Model
{
    protected $table = 'tbl_edict';

    protected $primaryKey = 'idx';

    protected $fillable = [
        'idx',
        'word',
        'detail'
    ];
}